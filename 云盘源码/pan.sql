/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50726
 Source Host           : localhost:3306
 Source Schema         : pan

 Target Server Type    : MySQL
 Target Server Version : 50726
 File Encoding         : 65001

 Date: 03/01/2023 09:11:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for myl_cash
-- ----------------------------
DROP TABLE IF EXISTS `myl_cash`;
CREATE TABLE `myl_cash`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT 0,
  `money` decimal(10, 2) NULL DEFAULT NULL,
  `amount` decimal(10, 2) NULL DEFAULT NULL,
  `state` int(1) NULL DEFAULT 0 COMMENT '0待处理 1已完成 2已拒绝',
  `ctime` int(10) NULL DEFAULT 0,
  `utime` int(10) NULL DEFAULT 0,
  `alipay_card` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `alipay_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_cash
-- ----------------------------

-- ----------------------------
-- Table structure for myl_downloads
-- ----------------------------
DROP TABLE IF EXISTS `myl_downloads`;
CREATE TABLE `myl_downloads`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `source` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `total_size` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `downloaded_size` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `g_id` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `speed` int(11) NULL DEFAULT NULL,
  `parent` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `attrs` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `error` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `dst` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `task_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `node_id` int(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_myl_downloads_deleted_at`(`deleted_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_downloads
-- ----------------------------
INSERT INTO `myl_downloads` VALUES (1, '2020-08-11 09:51:45', '2020-08-11 09:54:45', '2020-08-11 09:54:53', 1, 0, 'https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg', 0, 0, '68a16e187b954d43', 0, '/data/www/pan/tmp/aria2/1597110704997946676', '{\"gid\":\"68a16e187b954d43\",\"status\":\"active\",\"totalLength\":\"0\",\"completedLength\":\"0\",\"uploadLength\":\"0\",\"bitfield\":\"\",\"downloadSpeed\":\"0\",\"uploadSpeed\":\"0\",\"infoHash\":\"\",\"numSeeders\":\"\",\"seeder\":\"\",\"pieceLength\":\"1048576\",\"numPieces\":\"0\",\"connections\":\"1\",\"errorCode\":\"\",\"errorMessage\":\"\",\"followedBy\":null,\"belongsTo\":\"\",\"dir\":\"/data/www/pan/tmp/aria2/1597110704997946676\",\"files\":[{\"index\":\"1\",\"path\":\"\",\"length\":\"0\",\"completedLength\":\"0\",\"selected\":\"true\",\"uris\":[{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"used\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://www.tracymc.cn/wp-content/uploads/2018/11/15415776356273.jpg\",\"status\":\"waiting\"}]}],\"bittorrent\":{\"announceList\":null,\"comment\":\"\",\"creationDate\":0,\"mode\":\"\",\"info\":{\"name\":\"\"}}}', '', '/', 1, 0, NULL);
INSERT INTO `myl_downloads` VALUES (2, '2020-08-11 09:52:41', '2020-08-11 09:54:41', '2020-08-11 09:54:52', 1, 0, 'https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png', 0, 0, '3a686ce4110cc555', 0, '/data/www/pan/tmp/aria2/1597110761333435937', '{\"gid\":\"3a686ce4110cc555\",\"status\":\"active\",\"totalLength\":\"0\",\"completedLength\":\"0\",\"uploadLength\":\"0\",\"bitfield\":\"\",\"downloadSpeed\":\"0\",\"uploadSpeed\":\"0\",\"infoHash\":\"\",\"numSeeders\":\"\",\"seeder\":\"\",\"pieceLength\":\"1048576\",\"numPieces\":\"0\",\"connections\":\"1\",\"errorCode\":\"\",\"errorMessage\":\"\",\"followedBy\":null,\"belongsTo\":\"\",\"dir\":\"/data/www/pan/tmp/aria2/1597110761333435937\",\"files\":[{\"index\":\"1\",\"path\":\"\",\"length\":\"0\",\"completedLength\":\"0\",\"selected\":\"true\",\"uris\":[{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"used\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"}]}],\"bittorrent\":{\"announceList\":null,\"comment\":\"\",\"creationDate\":0,\"mode\":\"\",\"info\":{\"name\":\"\"}}}', '', '/', 1, 0, NULL);
INSERT INTO `myl_downloads` VALUES (3, '2020-08-11 09:55:05', '2020-08-11 09:56:05', '2020-08-11 09:56:55', 1, 0, 'https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png', 0, 0, '186fde1f9f758761', 0, '/data/www/pan/tmp/aria2/1597110904876801381', '{\"gid\":\"186fde1f9f758761\",\"status\":\"active\",\"totalLength\":\"0\",\"completedLength\":\"0\",\"uploadLength\":\"0\",\"bitfield\":\"\",\"downloadSpeed\":\"0\",\"uploadSpeed\":\"0\",\"infoHash\":\"\",\"numSeeders\":\"\",\"seeder\":\"\",\"pieceLength\":\"1048576\",\"numPieces\":\"0\",\"connections\":\"1\",\"errorCode\":\"\",\"errorMessage\":\"\",\"followedBy\":null,\"belongsTo\":\"\",\"dir\":\"/data/www/pan/tmp/aria2/1597110904876801381\",\"files\":[{\"index\":\"1\",\"path\":\"\",\"length\":\"0\",\"completedLength\":\"0\",\"selected\":\"true\",\"uris\":[{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"used\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png\",\"status\":\"waiting\"}]}],\"bittorrent\":{\"announceList\":null,\"comment\":\"\",\"creationDate\":0,\"mode\":\"\",\"info\":{\"name\":\"\"}}}', '', '/', 1, 0, NULL);
INSERT INTO `myl_downloads` VALUES (4, '2020-08-11 09:58:22', '2020-08-11 09:59:22', '2020-08-11 09:59:34', 1, 0, 'https://www.baidu.com/img/flexible/logo/pc/result.png', 0, 0, 'f109a798d5b191c4', 0, '/data/www/pan/tmp/aria2/1597111102021402528', '{\"gid\":\"f109a798d5b191c4\",\"status\":\"active\",\"totalLength\":\"0\",\"completedLength\":\"0\",\"uploadLength\":\"0\",\"bitfield\":\"\",\"downloadSpeed\":\"0\",\"uploadSpeed\":\"0\",\"infoHash\":\"\",\"numSeeders\":\"\",\"seeder\":\"\",\"pieceLength\":\"1048576\",\"numPieces\":\"0\",\"connections\":\"1\",\"errorCode\":\"\",\"errorMessage\":\"\",\"followedBy\":null,\"belongsTo\":\"\",\"dir\":\"/data/www/pan/tmp/aria2/1597111102021402528\",\"files\":[{\"index\":\"1\",\"path\":\"\",\"length\":\"0\",\"completedLength\":\"0\",\"selected\":\"true\",\"uris\":[{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"used\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"}]}],\"bittorrent\":{\"announceList\":null,\"comment\":\"\",\"creationDate\":0,\"mode\":\"\",\"info\":{\"name\":\"\"}}}', '', '/', 1, 0, NULL);
INSERT INTO `myl_downloads` VALUES (5, '2020-08-11 10:02:16', '2020-08-11 10:09:58', '2020-08-11 10:21:11', 5, 0, 'https://www.baidu.com/img/flexible/logo/pc/result.png', 0, 0, '2d220def6138344a', 0, '/data/www/pan/tmp/aria2/1597111335668567269', '{\"gid\":\"2d220def6138344a\",\"status\":\"removed\",\"totalLength\":\"0\",\"completedLength\":\"0\",\"uploadLength\":\"0\",\"bitfield\":\"\",\"downloadSpeed\":\"0\",\"uploadSpeed\":\"0\",\"infoHash\":\"\",\"numSeeders\":\"\",\"seeder\":\"\",\"pieceLength\":\"1048576\",\"numPieces\":\"0\",\"connections\":\"0\",\"errorCode\":\"31\",\"errorMessage\":\"\",\"followedBy\":null,\"belongsTo\":\"\",\"dir\":\"/data/www/pan/tmp/aria2/1597111335668567269\",\"files\":[{\"index\":\"1\",\"path\":\"\",\"length\":\"0\",\"completedLength\":\"0\",\"selected\":\"true\",\"uris\":[{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"used\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"},{\"uri\":\"https://www.baidu.com/img/flexible/logo/pc/result.png\",\"status\":\"waiting\"}]}],\"bittorrent\":{\"announceList\":null,\"comment\":\"\",\"creationDate\":0,\"mode\":\"\",\"info\":{\"name\":\"\"}}}', '', '/', 1, 0, NULL);
INSERT INTO `myl_downloads` VALUES (6, '2020-08-11 10:04:37', '2020-08-11 10:10:30', '2020-08-11 10:21:13', 5, 0, 'https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1', 0, 0, '42ae15ac8667ed77', 0, '/data/www/pan/tmp/aria2/1597111477177719748', '{\"gid\":\"42ae15ac8667ed77\",\"status\":\"removed\",\"totalLength\":\"0\",\"completedLength\":\"0\",\"uploadLength\":\"0\",\"bitfield\":\"\",\"downloadSpeed\":\"0\",\"uploadSpeed\":\"0\",\"infoHash\":\"\",\"numSeeders\":\"\",\"seeder\":\"\",\"pieceLength\":\"1048576\",\"numPieces\":\"0\",\"connections\":\"0\",\"errorCode\":\"31\",\"errorMessage\":\"\",\"followedBy\":null,\"belongsTo\":\"\",\"dir\":\"/data/www/pan/tmp/aria2/1597111477177719748\",\"files\":[{\"index\":\"1\",\"path\":\"\",\"length\":\"0\",\"completedLength\":\"0\",\"selected\":\"true\",\"uris\":[{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"used\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"},{\"uri\":\"https://csdnimg.cn/cdn/content-toolbar/csdn-logo.png?v=20200416.1\",\"status\":\"waiting\"}]}],\"bittorrent\":{\"announceList\":null,\"comment\":\"\",\"creationDate\":0,\"mode\":\"\",\"info\":{\"name\":\"\"}}}', '', '/', 1, 0, NULL);
INSERT INTO `myl_downloads` VALUES (7, '2020-08-11 10:20:55', '2020-08-11 10:20:55', '2020-08-11 10:21:15', 1, 0, 'https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg', 0, 0, 'cd30153fc3831ae4', 0, '/data/www/pan/tmp/aria2/1597112454923126253', '{\"gid\":\"cd30153fc3831ae4\",\"status\":\"active\",\"totalLength\":\"0\",\"completedLength\":\"0\",\"uploadLength\":\"0\",\"bitfield\":\"\",\"downloadSpeed\":\"0\",\"uploadSpeed\":\"0\",\"infoHash\":\"\",\"numSeeders\":\"\",\"seeder\":\"\",\"pieceLength\":\"1048576\",\"numPieces\":\"0\",\"connections\":\"1\",\"errorCode\":\"\",\"errorMessage\":\"\",\"followedBy\":null,\"belongsTo\":\"\",\"dir\":\"/data/www/pan/tmp/aria2/1597112454923126253\",\"files\":[{\"index\":\"1\",\"path\":\"\",\"length\":\"0\",\"completedLength\":\"0\",\"selected\":\"true\",\"uris\":[{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"used\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"}]}],\"bittorrent\":{\"announceList\":null,\"comment\":\"\",\"creationDate\":0,\"mode\":\"\",\"info\":{\"name\":\"\"}}}', '', '/', 1, 0, NULL);
INSERT INTO `myl_downloads` VALUES (8, '2020-08-11 10:22:55', '2020-08-11 10:22:55', '2020-08-11 10:36:06', 4, 0, 'https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg', 23176, 23176, '57040ff97638548c', 0, '/data/www/pan/tmp/aria2/1597112574885956492', '{\"gid\":\"57040ff97638548c\",\"status\":\"complete\",\"totalLength\":\"23176\",\"completedLength\":\"23176\",\"uploadLength\":\"0\",\"bitfield\":\"80\",\"downloadSpeed\":\"0\",\"uploadSpeed\":\"0\",\"infoHash\":\"\",\"numSeeders\":\"\",\"seeder\":\"\",\"pieceLength\":\"1048576\",\"numPieces\":\"1\",\"connections\":\"0\",\"errorCode\":\"0\",\"errorMessage\":\"\",\"followedBy\":null,\"belongsTo\":\"\",\"dir\":\"/data/www/pan/tmp/aria2/1597112574885956492\",\"files\":[{\"index\":\"1\",\"path\":\"/data/www/pan/tmp/aria2/1597112574885956492/default-avatar.jpg\",\"length\":\"23176\",\"completedLength\":\"23176\",\"selected\":\"true\",\"uris\":[{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"used\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"},{\"uri\":\"https://tenant-assets.meiqiausercontent.com/static-files/default-avatar.jpg\",\"status\":\"waiting\"}]}],\"bittorrent\":{\"announceList\":null,\"comment\":\"\",\"creationDate\":0,\"mode\":\"\",\"info\":{\"name\":\"\"}}}', '', '/', 1, 1, NULL);
INSERT INTO `myl_downloads` VALUES (9, '2020-08-11 10:32:21', '2020-08-11 10:35:17', '2020-08-11 10:36:07', 1, 0, 'magnet:?xt=urn:btih:832E9023149D9B8F19273551B14CEA3F1B16AC0E', 0, 0, '4b00728d09c00e64', 0, '/data/www/pan/tmp/aria2/1597113141124834370', '{\"gid\":\"4b00728d09c00e64\",\"status\":\"active\",\"totalLength\":\"0\",\"completedLength\":\"0\",\"uploadLength\":\"0\",\"bitfield\":\"\",\"downloadSpeed\":\"0\",\"uploadSpeed\":\"0\",\"infoHash\":\"832e9023149d9b8f19273551b14cea3f1b16ac0e\",\"numSeeders\":\"0\",\"seeder\":\"false\",\"pieceLength\":\"16384\",\"numPieces\":\"0\",\"connections\":\"0\",\"errorCode\":\"\",\"errorMessage\":\"\",\"followedBy\":null,\"belongsTo\":\"\",\"dir\":\"/data/www/pan/tmp/aria2/1597113141124834370\",\"files\":[{\"index\":\"1\",\"path\":\"[METADATA]832e9023149d9b8f19273551b14cea3f1b16ac0e\",\"length\":\"0\",\"completedLength\":\"0\",\"selected\":\"true\",\"uris\":[]}],\"bittorrent\":{\"announceList\":[[\"udp://tracker.coppersurfer.tk:6969/announce\"],[\"http://tracker.internetwarriors.net:1337/announce\"],[\"udp://tracker.opentrackr.org:1337/announce\"]],\"comment\":\"\",\"creationDate\":0,\"mode\":\"\",\"info\":{\"name\":\"\"}}}', '', '/', 1, 0, NULL);
INSERT INTO `myl_downloads` VALUES (10, '2020-08-11 10:36:17', '2020-08-11 10:36:17', '2020-08-11 10:37:03', 1, 0, 'magnet:?xt=urn:btih:6A42C1E3B10548409AFA29D5BFCBD238B69B44A1', 0, 0, 'abb8815fd5a27603', 0, '/data/www/pan/tmp/aria2/1597113376815728192', '{\"gid\":\"abb8815fd5a27603\",\"status\":\"active\",\"totalLength\":\"0\",\"completedLength\":\"0\",\"uploadLength\":\"0\",\"bitfield\":\"\",\"downloadSpeed\":\"0\",\"uploadSpeed\":\"0\",\"infoHash\":\"6a42c1e3b10548409afa29d5bfcbd238b69b44a1\",\"numSeeders\":\"0\",\"seeder\":\"false\",\"pieceLength\":\"16384\",\"numPieces\":\"0\",\"connections\":\"0\",\"errorCode\":\"\",\"errorMessage\":\"\",\"followedBy\":null,\"belongsTo\":\"\",\"dir\":\"/data/www/pan/tmp/aria2/1597113376815728192\",\"files\":[{\"index\":\"1\",\"path\":\"[METADATA]6a42c1e3b10548409afa29d5bfcbd238b69b44a1\",\"length\":\"0\",\"completedLength\":\"0\",\"selected\":\"true\",\"uris\":[]}],\"bittorrent\":{\"announceList\":[[\"udp://tracker.coppersurfer.tk:6969/announce\"],[\"http://tracker.internetwarriors.net:1337/announce\"],[\"udp://tracker.opentrackr.org:1337/announce\"]],\"comment\":\"\",\"creationDate\":0,\"mode\":\"\",\"info\":{\"name\":\"\"}}}', '', '/', 1, 0, NULL);
INSERT INTO `myl_downloads` VALUES (11, '2020-08-11 10:37:12', '2020-08-11 10:37:58', '2020-08-11 10:38:55', 1, 0, 'magnet:?xt=urn:btih:4C1056A2E580BD643AB77070C26021BCEBE67EAF', 0, 0, '1ca3b0900cf70241', 0, '/data/www/pan/tmp/aria2/1597113431704561277', '{\"gid\":\"60317c185176ec98\",\"status\":\"active\",\"totalLength\":\"0\",\"completedLength\":\"0\",\"uploadLength\":\"0\",\"bitfield\":\"\",\"downloadSpeed\":\"0\",\"uploadSpeed\":\"0\",\"infoHash\":\"4c1056a2e580bd643ab77070c26021bcebe67eaf\",\"numSeeders\":\"0\",\"seeder\":\"false\",\"pieceLength\":\"16384\",\"numPieces\":\"0\",\"connections\":\"0\",\"errorCode\":\"\",\"errorMessage\":\"\",\"followedBy\":null,\"belongsTo\":\"\",\"dir\":\"/data/www/pan/tmp/aria2/1597113431704561277\",\"files\":[{\"index\":\"1\",\"path\":\"[METADATA]4c1056a2e580bd643ab77070c26021bcebe67eaf\",\"length\":\"0\",\"completedLength\":\"0\",\"selected\":\"true\",\"uris\":[]}],\"bittorrent\":{\"announceList\":[[\"udp://tracker.coppersurfer.tk:6969/announce\"],[\"http://tracker.internetwarriors.net:1337/announce\"],[\"udp://tracker.opentrackr.org:1337/announce\"]],\"comment\":\"\",\"creationDate\":0,\"mode\":\"\",\"info\":{\"name\":\"\"}}}', '', '/', 1, 0, NULL);
INSERT INTO `myl_downloads` VALUES (12, '2020-08-11 10:47:34', '2020-08-11 12:37:49', '2020-08-11 12:38:12', 5, 0, 'magnet:?xt=urn:btih:51F635A6EBC1F69E548F59E67423FF6EDB0C22E1&dn=周杰伦-菊花台-国语-2806279_MTV分享精灵_MTVP2P.mpg', 0, 0, '895f14320c6fd380', 0, '/data/www/pan/tmp/aria2/1597114054365580832', '{\"gid\":\"895f14320c6fd380\",\"status\":\"removed\",\"totalLength\":\"0\",\"completedLength\":\"0\",\"uploadLength\":\"0\",\"bitfield\":\"\",\"downloadSpeed\":\"0\",\"uploadSpeed\":\"0\",\"infoHash\":\"51f635a6ebc1f69e548f59e67423ff6edb0c22e1\",\"numSeeders\":\"0\",\"seeder\":\"\",\"pieceLength\":\"16384\",\"numPieces\":\"0\",\"connections\":\"0\",\"errorCode\":\"31\",\"errorMessage\":\"\",\"followedBy\":null,\"belongsTo\":\"\",\"dir\":\"/data/www/pan/tmp/aria2/1597114054365580832\",\"files\":[{\"index\":\"1\",\"path\":\"[METADATA]周杰伦-菊花台-国语-2806279_MTV分享精灵_MTVP2P.mpg\",\"length\":\"0\",\"completedLength\":\"0\",\"selected\":\"true\",\"uris\":[]}],\"bittorrent\":{\"announceList\":[[\"udp://9.rarbg.com:2710/announce\"],[\"udp://tracker.skyts.net:6969/announce\"]],\"comment\":\"\",\"creationDate\":0,\"mode\":\"\",\"info\":{\"name\":\"\"}}}', '', '/', 1, 0, NULL);

-- ----------------------------
-- Table structure for myl_files
-- ----------------------------
DROP TABLE IF EXISTS `myl_files`;
CREATE TABLE `myl_files`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `source_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `size` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `pic_info` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `folder_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `policy_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `upload_session_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `metadata` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_only_one`(`name`, `user_id`, `folder_id`) USING BTREE,
  UNIQUE INDEX `session_only_one`(`upload_session_id`) USING BTREE,
  INDEX `idx_myl_files_deleted_at`(`deleted_at`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `folder_id`(`folder_id`) USING BTREE,
  INDEX `session_id`(`upload_session_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_files
-- ----------------------------
INSERT INTO `myl_files` VALUES (2, '2020-08-10 20:19:41', '2022-05-05 20:01:27', NULL, 'key.bin', 'uploads/1/key.bin', 1, 2416, '0,0', 8, 2, NULL, NULL);
INSERT INTO `myl_files` VALUES (6, '2020-08-11 10:22:57', '2022-05-05 20:01:27', NULL, 'default-avatar.jpg', 'uploads/1/default-avatar.jpg', 1, 23176, '0,0', 1, 2, NULL, NULL);
INSERT INTO `myl_files` VALUES (7, '2020-08-11 10:25:13', '2022-05-05 20:01:27', NULL, '图怪兽_f609f7ddd9943913d45e825052c19783_33192.png', 'uploads/1/图怪兽_f609f7ddd9943913d45e825052c19783_33192.png', 1, 27753, '0,0', 1, 2, NULL, NULL);
INSERT INTO `myl_files` VALUES (8, '2020-08-11 10:29:13', '2022-05-05 20:01:27', NULL, 'pan编译后.zip', 'uploads/1/pan编译后.zip', 1, 66665505, '0,0', 1, 2, NULL, NULL);
INSERT INTO `myl_files` VALUES (9, '2020-08-20 17:31:33', '2022-05-05 20:01:27', NULL, 'oss.png', 'uploads/1/1_RY39ilRp_oss.png', 1, 21340, '0,0', 1, 1, NULL, NULL);
INSERT INTO `myl_files` VALUES (10, '2021-11-19 17:09:38', '2022-05-05 20:01:27', NULL, '~微信图片_20211119143842.jpg', 'uploads/1/1_oScyGVbK_微信图片_20211119143842.jpg', 1, 143708, '0,0', 1, 1, NULL, NULL);
INSERT INTO `myl_files` VALUES (11, '2022-05-05 18:54:01', '2022-05-05 20:01:27', NULL, 'video - 副本.mp4', 'uploads/1/video_-_副本.mp4', 1, 27602948, '0,0', 1, 2, NULL, 'null');
INSERT INTO `myl_files` VALUES (15, '2022-05-05 20:18:28', '2022-05-05 18:52:55', NULL, 'video.mp4', 'uploads/1/video.mp4', 1, 27602948, '0,0', 1, 2, NULL, 'null');
INSERT INTO `myl_files` VALUES (16, '2022-06-17 10:11:55', '2022-06-17 10:11:37', NULL, '3333.mp3', 'uploads/1/3333.mp3', 1, 4238150, '0,0', 1, 2, NULL, 'null');
INSERT INTO `myl_files` VALUES (18, '2022-07-15 15:52:57', '2022-03-03 11:20:17', NULL, 'net.js', 'uploads/4/net.js', 4, 11106, '0,0', 5, 2, NULL, 'null');

-- ----------------------------
-- Table structure for myl_folders
-- ----------------------------
DROP TABLE IF EXISTS `myl_folders`;
CREATE TABLE `myl_folders`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `parent_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `owner_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `policy_id` int(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_only_one_name`(`name`, `parent_id`) USING BTREE,
  INDEX `idx_myl_folders_deleted_at`(`deleted_at`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  INDEX `owner_id`(`owner_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_folders
-- ----------------------------
INSERT INTO `myl_folders` VALUES (1, '2020-08-10 19:58:45', '2022-05-05 18:15:35', NULL, '/', NULL, 1, 2);
INSERT INTO `myl_folders` VALUES (3, '2020-08-12 11:16:05', '2020-08-12 11:16:05', NULL, '/', NULL, 2, 0);
INSERT INTO `myl_folders` VALUES (4, '2020-08-18 12:26:20', '2020-08-18 12:26:20', NULL, '/', NULL, 3, 0);
INSERT INTO `myl_folders` VALUES (5, '2020-08-18 12:27:37', '2022-07-15 15:52:41', NULL, '/', NULL, 4, 2);
INSERT INTO `myl_folders` VALUES (6, '2020-08-18 13:46:10', '2020-08-18 13:46:10', NULL, '/', NULL, 5, 0);
INSERT INTO `myl_folders` VALUES (7, '2021-08-05 19:50:34', '2021-08-05 19:50:34', NULL, '/', NULL, 6, 0);
INSERT INTO `myl_folders` VALUES (8, '2021-09-01 13:07:04', '2021-09-01 13:07:04', NULL, '333', 1, 1, 0);
INSERT INTO `myl_folders` VALUES (9, '2021-11-29 13:59:36', '2021-11-29 13:59:36', NULL, '/', NULL, 7, 0);
INSERT INTO `myl_folders` VALUES (10, '2021-11-29 15:07:37', '2021-11-29 15:07:37', NULL, '/', NULL, 8, 0);
INSERT INTO `myl_folders` VALUES (11, '2022-05-05 18:56:16', '2022-05-05 18:56:16', NULL, '/', NULL, 9, 0);

-- ----------------------------
-- Table structure for myl_game_duobao
-- ----------------------------
DROP TABLE IF EXISTS `myl_game_duobao`;
CREATE TABLE `myl_game_duobao`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '游戏名称',
  `gift_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '礼物图片',
  `gift_id` int(11) NULL DEFAULT 0 COMMENT '奖品id',
  `gift_price` int(11) NULL DEFAULT 0 COMMENT '礼物价值',
  `type` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '奖品类型 gift是礼物 card是卡密',
  `need_num` int(11) NULL DEFAULT 0 COMMENT '需要投注数量',
  `price` int(11) NULL DEFAULT 0 COMMENT '投资金额',
  `lssue` int(11) NULL DEFAULT 0 COMMENT '当前期号',
  `max_lssue` int(11) NULL DEFAULT 0 COMMENT '最大期号',
  `max_num` int(11) NULL DEFAULT 0 COMMENT '单场最多投资数量0不限',
  `state` int(1) NULL DEFAULT 0 COMMENT '状态0正常 1禁用',
  `mark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注说明',
  `sort` int(11) NULL DEFAULT 0 COMMENT '排序 ，越小越靠前',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_game_duobao
-- ----------------------------
INSERT INTO `myl_game_duobao` VALUES (1, '魔法棒', NULL, 1, 0, 'gift', 11, 0, 1, 0, 0, 1, '一人独享，中奖后礼物自动发送到背包中，可打赏给喜欢的文件分享。', 10);
INSERT INTO `myl_game_duobao` VALUES (2, '麦克风', NULL, 2, 0, 'gift', 22, 0, 1, 0, 0, 1, '一人独享，中奖后礼物自动发送到背包中，可打赏给喜欢的文件分享。', 20);
INSERT INTO `myl_game_duobao` VALUES (3, '[免费场]VIP会员1月', '/upload/gift/vip1.png', 1, 10, 'card', 20, 0, 7, 10, 1, 0, '一人独享，中奖后在投资记录中查看卡密，购买会员页面使用激活码兑换。', 30);
INSERT INTO `myl_game_duobao` VALUES (4, '[免费场]VIP会员1季', '/upload/gift/vip3.png', 2, 27, 'card', 100, 0, 1, 10, 1, 0, '一人独享，中奖后在投资记录中查看卡密，购买会员页面使用激活码兑换。', 40);
INSERT INTO `myl_game_duobao` VALUES (5, '[免费场]50积分', '/upload/gift/jifen50.png', 3, 5, 'card', 100, 0, 1, 10, 1, 0, '一人独享，中奖后在投资记录中查看卡密，购买会员页面使用激活码兑换。', 50);

-- ----------------------------
-- Table structure for myl_game_duobao_issue
-- ----------------------------
DROP TABLE IF EXISTS `myl_game_duobao_issue`;
CREATE TABLE `myl_game_duobao_issue`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NULL DEFAULT 0 COMMENT '游戏id',
  `iid` int(11) NULL DEFAULT 0 COMMENT '期号',
  `num` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '中奖号码',
  `user_id` int(11) NULL DEFAULT 0 COMMENT '中奖用户id',
  `num_a` varchar(6) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '数值A',
  `num_b` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '数值B',
  `ctime` int(10) NULL DEFAULT 0 COMMENT '创建时间',
  `utime` int(10) NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_game_duobao_issue
-- ----------------------------
INSERT INTO `myl_game_duobao_issue` VALUES (1, 3, 1, '1000019', 1, '105416', '94922', 1605849686, 1606097348);
INSERT INTO `myl_game_duobao_issue` VALUES (2, 4, 1, NULL, 0, '', '', 1605850326, 0);
INSERT INTO `myl_game_duobao_issue` VALUES (3, 5, 1, NULL, 0, '', '', 1605850370, 0);
INSERT INTO `myl_game_duobao_issue` VALUES (4, 3, 2, '1000007', 1, '054233', '01893', 1606097348, 1606097498);
INSERT INTO `myl_game_duobao_issue` VALUES (5, 3, 3, '1000011', 1, '008165', '28745', 1606097498, 1606097799);
INSERT INTO `myl_game_duobao_issue` VALUES (6, 3, 4, '1000010', 1, '964792', '70877', 1606097799, 1606097836);
INSERT INTO `myl_game_duobao_issue` VALUES (7, 3, 5, '1000004', 1, '921966', '70877', 1606097836, 1606097863);
INSERT INTO `myl_game_duobao_issue` VALUES (8, 3, 6, '1000004', 1, '879822', '93421', 1606097863, 1606097900);
INSERT INTO `myl_game_duobao_issue` VALUES (9, 3, 7, NULL, 0, '', '', 1606097900, 0);

-- ----------------------------
-- Table structure for myl_game_duobao_log
-- ----------------------------
DROP TABLE IF EXISTS `myl_game_duobao_log`;
CREATE TABLE `myl_game_duobao_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sid` int(11) NULL DEFAULT 0 COMMENT '游戏id',
  `iid` int(11) NULL DEFAULT 0 COMMENT '期号',
  `num` int(10) NULL DEFAULT 0 COMMENT '幸运号',
  `user_id` int(11) NULL DEFAULT 0 COMMENT '用户id',
  `ctime` int(10) NULL DEFAULT 0 COMMENT '投资时间',
  `mark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 131 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_game_duobao_log
-- ----------------------------
INSERT INTO `myl_game_duobao_log` VALUES (128, 3, 7, 1000001, 1, 1606100318, NULL);
INSERT INTO `myl_game_duobao_log` VALUES (129, 3, 7, 1000002, 1, 1606100321, NULL);
INSERT INTO `myl_game_duobao_log` VALUES (130, 4, 1, 1000001, 1, 1606100353, NULL);

-- ----------------------------
-- Table structure for myl_gift
-- ----------------------------
DROP TABLE IF EXISTS `myl_gift`;
CREATE TABLE `myl_gift`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gift_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '礼物名称',
  `gift_price` int(11) NULL DEFAULT 0 COMMENT '礼物价格',
  `gift_pic` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '礼物图片',
  `is_show` int(1) NULL DEFAULT 0 COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_gift
-- ----------------------------
INSERT INTO `myl_gift` VALUES (1, '玫瑰花', 1, '/upload/gift/1588144425.png', 0);
INSERT INTO `myl_gift` VALUES (2, '魔法棒', 10, '/upload/gift/1574486045.png', 0);
INSERT INTO `myl_gift` VALUES (3, '麦克风', 20, '/upload/gift/1574486017.png', 0);
INSERT INTO `myl_gift` VALUES (4, '丘比特', 50, '/upload/gift/1574486103.png', 0);
INSERT INTO `myl_gift` VALUES (5, '巧克力', 100, '/upload/gift/1574486158.png', 0);
INSERT INTO `myl_gift` VALUES (6, '泡泡机', 500, '/upload/gift/1574486299.png', 0);
INSERT INTO `myl_gift` VALUES (7, '许愿瓶', 1000, '/upload/gift/1574850031.png', 0);

-- ----------------------------
-- Table structure for myl_giftcard
-- ----------------------------
DROP TABLE IF EXISTS `myl_giftcard`;
CREATE TABLE `myl_giftcard`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '卡号',
  `card_pass` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '密码',
  `type_id` int(11) NULL DEFAULT 0 COMMENT '类型id',
  `user_id` int(11) NULL DEFAULT 0 COMMENT '所属用户id',
  `ctime` int(10) NULL DEFAULT 0 COMMENT '创建时间',
  `utime` int(10) NULL DEFAULT 0 COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 31 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_giftcard
-- ----------------------------
INSERT INTO `myl_giftcard` VALUES (1, 'e016fae1-9ad1-4e90-ab7a-f555f8f35140', NULL, 1, 1, 1605837265, 1605837265);
INSERT INTO `myl_giftcard` VALUES (2, '76434fca-4434-4328-8f7c-8cce814bca5b', NULL, 1, 1, 1605837265, 1606097498);
INSERT INTO `myl_giftcard` VALUES (3, 'c62ebbd4-d62d-43f1-82ab-839e97ae5b25', NULL, 1, 1, 1605837265, 1606097799);
INSERT INTO `myl_giftcard` VALUES (4, '0d49612f-6346-43b6-bfd3-f3b9cb2f3447', NULL, 1, 1, 1605837265, 1606097836);
INSERT INTO `myl_giftcard` VALUES (5, '78a70707-fe1f-4e1d-ab37-dad1920d2371', NULL, 1, 1, 1605837265, 1606097863);
INSERT INTO `myl_giftcard` VALUES (6, '8fd1de2c-18a6-4baa-97f9-a7188a3432c1', NULL, 1, 1, 1605837265, 1606097900);
INSERT INTO `myl_giftcard` VALUES (7, '29d3054d-da3b-4484-ac4e-e76ec3bfd329', NULL, 1, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (8, 'fa611b13-ef26-47e0-a2a3-4c7dedeec361', NULL, 1, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (9, '0b78e7ea-e38a-45aa-9b1b-2e3b2d9ecd3a', NULL, 1, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (10, '7072e6af-8b70-4717-9c55-4a05d6f32fc2', NULL, 1, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (11, '405d7534-f93e-47a4-9a4d-fc649c3018ce', NULL, 2, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (12, 'f5b57e88-6dbe-4d44-ac2c-924c6efa0708', NULL, 2, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (13, 'c5ccf2f4-5245-4f77-beeb-7f3eaadcfbc3', NULL, 2, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (14, '0adec320-48b8-458b-893b-2be0031fd494', NULL, 2, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (15, '010b6efb-1d64-4fc2-8d00-8d6134b1b890', NULL, 2, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (16, '0b0cfe2e-b0dd-45cb-a277-1c1cfc0635e7', NULL, 2, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (17, '7370d2a3-2ede-4ed4-b90c-360c8c7fb124', NULL, 2, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (18, '10a8afbf-b23e-47e2-a8d4-aa68b2946e37', NULL, 2, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (19, 'bf0ed38a-f1b5-4b01-b17c-41435f0ab2df', NULL, 2, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (20, '0c2835b3-1525-4103-a30f-e3c8dd7e7747', NULL, 2, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (21, '952a5b4f-e646-415d-ab5a-830a7b28074f', NULL, 3, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (22, '00e4cda4-195d-414f-b9d9-4598842805c8', NULL, 3, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (23, '74db563e-7d7f-4fce-9a37-abc3f31a2681', NULL, 3, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (24, 'db49ff2f-f720-40b0-9021-375938ef29ec', NULL, 3, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (25, 'ea6a6abf-90ce-44ed-9177-185dc47eec7e', NULL, 3, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (26, '04b90e94-33f9-4301-a3ba-81f98586b1a3', NULL, 3, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (27, '5a2efa6d-c14f-474e-9b89-bf57f8a35f56', NULL, 3, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (28, '84331e7b-63be-4c8a-903f-d05c45a605d2', NULL, 3, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (29, 'b353d93d-3f2a-453d-8f1b-c85d444a6302', NULL, 3, 0, 1605837265, 0);
INSERT INTO `myl_giftcard` VALUES (30, '2350d200-2755-477d-86e0-7c4832dfbd62', NULL, 3, 0, 1605837265, 0);

-- ----------------------------
-- Table structure for myl_groups
-- ----------------------------
DROP TABLE IF EXISTS `myl_groups`;
CREATE TABLE `myl_groups`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `policies` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `max_storage` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `share_enabled` tinyint(1) NULL DEFAULT NULL,
  `web_dav_enabled` tinyint(1) NULL DEFAULT NULL,
  `speed_limit` int(11) NULL DEFAULT NULL,
  `options` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_myl_groups_deleted_at`(`deleted_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_groups
-- ----------------------------
INSERT INTO `myl_groups` VALUES (1, '2020-08-10 19:58:45', '2022-05-05 18:17:21', NULL, '管理员', '[1,2]', 1099511627776, 1, 1, 0, '{\"archive_download\":true,\"archive_task\":true,\"share_download\":true,\"share_free\":true,\"aria2\":true,\"source_batch\":1}');
INSERT INTO `myl_groups` VALUES (2, '2020-08-10 19:58:45', '2021-11-29 15:47:03', NULL, '注册会员', '[1,2]', 1099511627776, 1, 0, 0, '{\"share_download\":true}');
INSERT INTO `myl_groups` VALUES (3, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, '游客', '[]', 0, 0, 0, 0, '{\"share_download\":true}');
INSERT INTO `myl_groups` VALUES (4, '2020-08-10 20:05:55', '2020-08-20 17:31:04', NULL, 'VIP会员', '[1]', 1099511627776, 1, 1, 0, '{\"archive_download\":true,\"archive_task\":true,\"compress_size\":1073741824,\"decompress_size\":1073741824,\"share_download\":true,\"share_free\":true,\"aria2\":true}');

-- ----------------------------
-- Table structure for myl_lock
-- ----------------------------
DROP TABLE IF EXISTS `myl_lock`;
CREATE TABLE `myl_lock`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `index`(`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_lock
-- ----------------------------
INSERT INTO `myl_lock` VALUES (1, '夺宝达人');

-- ----------------------------
-- Table structure for myl_money_log
-- ----------------------------
DROP TABLE IF EXISTS `myl_money_log`;
CREATE TABLE `myl_money_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `mark` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `money` int(11) NULL DEFAULT 0,
  `money_old` int(11) NULL DEFAULT 0,
  `ctime` int(10) NULL DEFAULT 0,
  `order_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_money_log
-- ----------------------------
INSERT INTO `myl_money_log` VALUES (1, NULL, NULL, 0, 0, 0, NULL, 1);
INSERT INTO `myl_money_log` VALUES (2, '增值服务', '积分购买增值服务比邻云盘 - 10GB扩容包(联系客服免费扩容)', -10, 390, 1597742428, '20200818172028887059', 1);
INSERT INTO `myl_money_log` VALUES (3, '付费下载', '积分付费下载文件key.bin', -100, 900, 1597742522, '', 5);
INSERT INTO `myl_money_log` VALUES (4, '下载分成', '积分分成下载文件key.bin', 90, 480, 1597742522, '', 1);
INSERT INTO `myl_money_log` VALUES (5, '积分充值', '在线积分充值', 100, 1000, 1597742598, '', 5);
INSERT INTO `myl_money_log` VALUES (6, '增值服务', '积分购买增值服务比邻云盘 - 100GB扩容包(联系客服免费扩容)', -100, 1000, 1597744854, '20200818180054675999', 5);
INSERT INTO `myl_money_log` VALUES (7, '邀请分成', '邀请的下级用户积分消费分成', -5, 480, 1597744854, '20200818180054675999', 1);
INSERT INTO `myl_money_log` VALUES (8, '增值服务', '积分购买增值服务比邻云盘 - 100GB扩容包(联系客服免费扩容)', -100, 900, 1597745206, '20200818180646101398', 5);
INSERT INTO `myl_money_log` VALUES (9, '邀请分成', '邀请的下级用户积分消费分成', 5, 480, 1597745206, '20200818180646101398', 1);
INSERT INTO `myl_money_log` VALUES (10, '增值服务', '积分购买增值服务比邻云盘 - 100GB扩容包(联系客服免费扩容)', -100, 800, 1597745507, '20200818181147135260', 5);
INSERT INTO `myl_money_log` VALUES (11, '邀请分成', '邀请的下级用户积分消费分成', 5, 480, 1597745507, '20200818181147135260', 1);
INSERT INTO `myl_money_log` VALUES (12, '增值服务', '积分购买增值服务比邻云盘 - 100GB扩容包(联系客服免费扩容)', -100, 1000, 1597745691, '20200818181451187484', 3);
INSERT INTO `myl_money_log` VALUES (13, '增值服务', '积分购买增值服务比邻云盘 - VIP会员月度', -100, 900, 1597891052, '20200820103732596670', 3);
INSERT INTO `myl_money_log` VALUES (14, '增值服务', '积分购买增值服务比邻云盘 - VIP会员月度', -100, 700, 1597891116, '20200820103836619797', 5);
INSERT INTO `myl_money_log` VALUES (15, '邀请分成', '下级用户[test3]积分消费分成', 5, 485, 1597891116, '20200820103836619797', 1);
INSERT INTO `myl_money_log` VALUES (16, 'VIP分成', '下级用户[test3]升级VIP会员分成', 200, 490, 1597891116, '', 1);
INSERT INTO `myl_money_log` VALUES (17, '夺宝达人', '参与[免费场]VIP会员1月第0期夺宝达人，花费690积分。', -690, 690, 1605849686, '', 1);
INSERT INTO `myl_money_log` VALUES (18, '夺宝达人', '参与[免费场]VIP会员1月第1期夺宝达人，花费1积分。', -1, 1000, 1605850240, '', 1);
INSERT INTO `myl_money_log` VALUES (19, '夺宝达人', '参与[免费场]VIP会员1月第1期夺宝达人，花费1积分。', -1, 999, 1605850255, '', 1);
INSERT INTO `myl_money_log` VALUES (20, '夺宝达人', '参与[免费场]VIP会员1月第1期夺宝达人，花费1积分。', -1, 998, 1605850401, '', 1);
INSERT INTO `myl_money_log` VALUES (21, '夺宝达人', '参与[免费场]VIP会员1月第1期夺宝达人，花费1积分。', -1, 997, 1605850405, '', 1);
INSERT INTO `myl_money_log` VALUES (22, '夺宝达人', '参与[免费场]VIP会员1月第1期夺宝达人，花费1积分。', -1, 996, 1605850411, '', 1);
INSERT INTO `myl_money_log` VALUES (23, '夺宝达人', '参与[免费场]VIP会员1月第1期夺宝达人，花费1积分。', -1, 995, 1605850412, '', 1);
INSERT INTO `myl_money_log` VALUES (24, '夺宝达人', '参与[免费场]VIP会员1月第1期夺宝达人，花费1积分。', -1, 994, 1605850413, '', 1);
INSERT INTO `myl_money_log` VALUES (25, '购买礼物', '购买礼物巧克力*1，花费100积分。', -100, 993, 1627353564, '', 1);
INSERT INTO `myl_money_log` VALUES (26, '增值服务', '积分购买增值服务比邻云盘 - VIP会员月度', -100, 893, 1637896826, '20211126112026100079', 1);
INSERT INTO `myl_money_log` VALUES (27, '增值服务', '积分购买增值服务比邻云盘 - 100GB扩容包(联系客服免费扩容)', -100, 700, 1651748241, '20220505185721449077', 1);
INSERT INTO `myl_money_log` VALUES (28, '增值服务', '积分购买增值服务比邻云盘 - 100GB扩容包(联系客服免费扩容)', -100, 100, 1651748266, '20220505185746420643', 9);
INSERT INTO `myl_money_log` VALUES (29, '邀请分成', '下级用户[test111]积分消费分成', 5, 600, 1651748266, '20220505185746420643', 1);

-- ----------------------------
-- Table structure for myl_my_gift
-- ----------------------------
DROP TABLE IF EXISTS `myl_my_gift`;
CREATE TABLE `myl_my_gift`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gift_id` int(11) NULL DEFAULT 0,
  `gift_num` int(11) NULL DEFAULT 0,
  `user_id` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_my_gift
-- ----------------------------
INSERT INTO `myl_my_gift` VALUES (1, 5, 1, 1);

-- ----------------------------
-- Table structure for myl_my_gift_log
-- ----------------------------
DROP TABLE IF EXISTS `myl_my_gift_log`;
CREATE TABLE `myl_my_gift_log`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `gift_id` int(11) NULL DEFAULT 0,
  `gift_num` int(11) NULL DEFAULT 0,
  `fid` int(11) NULL DEFAULT 0,
  `ctime` int(10) NULL DEFAULT 0,
  `user_id` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_my_gift_log
-- ----------------------------
INSERT INTO `myl_my_gift_log` VALUES (1, '购买礼物', 5, 1, 0, 1627353564, 1);

-- ----------------------------
-- Table structure for myl_nodes
-- ----------------------------
DROP TABLE IF EXISTS `myl_nodes`;
CREATE TABLE `myl_nodes`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `server` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL,
  `slave_key` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `master_key` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `aria2_enabled` tinyint(1) NULL DEFAULT NULL,
  `aria2_options` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL,
  `rank` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_myl_nodes_deleted_at`(`deleted_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of myl_nodes
-- ----------------------------
INSERT INTO `myl_nodes` VALUES (1, '2021-11-26 11:11:01', '2021-11-26 11:11:01', NULL, 0, '主机（本机）', 1, '', '', '', 1, '{\"server\":\"http://127.0.0.1:6800\",\"token\":\"a8387f118295e3433109\",\"temp_path\":\"/data/www/pan/temp/\",\"options\":\"{}\",\"interval\":60,\"timeout\":10}', 0);

-- ----------------------------
-- Table structure for myl_orders
-- ----------------------------
DROP TABLE IF EXISTS `myl_orders`;
CREATE TABLE `myl_orders`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `order_no` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `method` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `product_id` bigint(20) NULL DEFAULT NULL,
  `num` int(11) NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `price` int(11) NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_myl_orders_deleted_at`(`deleted_at`) USING BTREE,
  INDEX `order_number`(`order_no`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_orders
-- ----------------------------
INSERT INTO `myl_orders` VALUES (1, '2020-08-11 13:46:22', '2020-08-11 13:46:54', NULL, 1, '20200811134621918048', 2, 'alipay', 0, 1, '比邻云盘 - 1 积分', 10, 1);
INSERT INTO `myl_orders` VALUES (2, '2020-08-11 14:16:33', '2020-08-11 14:16:33', NULL, 1, '20200811141633905274', 1, 'alipay', 1597126248456, 1, '比邻云盘 - VIP会员季度', 2500, 0);
INSERT INTO `myl_orders` VALUES (3, '2020-08-12 16:33:04', '2020-08-12 16:33:04', NULL, 1, '20200812163303491751', 0, 'payjs', 1597127262962, 1, '比邻云盘 - 1TB扩容包(联系客服免费扩容)', 10000, 0);
INSERT INTO `myl_orders` VALUES (4, '2020-08-13 11:28:22', '2020-08-13 11:28:22', NULL, 1, '20200813112821444449', 0, 'alipay', 1597127200064, 1, '比邻云盘 - 10GB扩容包(联系客服免费扩容)', 100, 0);
INSERT INTO `myl_orders` VALUES (5, '2020-08-13 11:28:57', '2020-08-13 11:28:57', NULL, 1, '20200813112856383728', 0, 'alipay', 1597127328232, 1, '比邻云盘 - 2TB扩容包(联系客服免费扩容)', 15000, 0);
INSERT INTO `myl_orders` VALUES (6, '2020-08-13 12:07:33', '2020-08-13 12:07:33', NULL, 1, '20200813120732305272', 0, 'alipay', 1597127328232, 1, '比邻云盘 - 2TB扩容包(联系客服免费扩容)', 15000, 0);
INSERT INTO `myl_orders` VALUES (7, '2020-08-13 12:20:14', '2020-08-13 12:20:14', NULL, 1, '20200813122013849260', 0, 'payjs', 1597127200064, 1, '比邻云盘 - 10GB扩容包(联系客服免费扩容)', 100, 0);
INSERT INTO `myl_orders` VALUES (8, '2020-08-13 12:20:29', '2020-08-13 12:20:29', NULL, 1, '20200813122029962061', 0, 'alipay', 1597127200064, 1, '比邻云盘 - 10GB扩容包(联系客服免费扩容)', 100, 0);
INSERT INTO `myl_orders` VALUES (9, '2020-08-13 12:20:37', '2020-08-13 12:20:37', NULL, 1, '20200813122036341104', 0, 'payjs', 1597127200064, 1, '比邻云盘 - 10GB扩容包(联系客服免费扩容)', 100, 0);
INSERT INTO `myl_orders` VALUES (10, '2020-08-13 13:57:54', '2020-08-13 13:57:54', NULL, 1, '20200813135753314061', 2, 'payjs', 0, 1, '比邻云盘 - 1 积分', 10, 0);
INSERT INTO `myl_orders` VALUES (11, '2020-08-13 13:58:14', '2020-08-13 14:04:15', NULL, 1, '20200813135813338773', 2, 'payjs', 0, 1, '比邻云盘 - 1 积分', 10, 1);
INSERT INTO `myl_orders` VALUES (12, '2020-08-18 17:20:28', '2020-08-18 17:20:28', NULL, 1, '20200818172028887059', 0, 'score', 1597127200064, 1, '比邻云盘 - 10GB扩容包(联系客服免费扩容)', 10, 1);
INSERT INTO `myl_orders` VALUES (13, '2020-08-18 18:00:54', '2020-08-18 18:00:54', NULL, 5, '20200818180054675999', 0, 'score', 1597127237595, 1, '比邻云盘 - 100GB扩容包(联系客服免费扩容)', 100, 1);
INSERT INTO `myl_orders` VALUES (14, '2020-08-18 18:06:46', '2020-08-18 18:06:46', NULL, 5, '20200818180646101398', 0, 'score', 1597127237595, 1, '比邻云盘 - 100GB扩容包(联系客服免费扩容)', 100, 1);
INSERT INTO `myl_orders` VALUES (15, '2020-08-18 18:11:48', '2020-08-18 18:11:48', NULL, 5, '20200818181147135260', 0, 'score', 1597127237595, 1, '比邻云盘 - 100GB扩容包(联系客服免费扩容)', 100, 1);
INSERT INTO `myl_orders` VALUES (16, '2020-08-18 18:14:52', '2020-08-18 18:14:52', NULL, 3, '20200818181451187484', 0, 'score', 1597127237595, 1, '比邻云盘 - 100GB扩容包(联系客服免费扩容)', 100, 1);
INSERT INTO `myl_orders` VALUES (17, '2020-08-20 10:37:32', '2020-08-20 10:37:32', NULL, 3, '20200820103732596670', 1, 'score', 1597126201955, 1, '比邻云盘 - VIP会员月度', 100, 1);
INSERT INTO `myl_orders` VALUES (18, '2020-08-20 10:38:36', '2020-08-20 10:38:36', NULL, 5, '20200820103836619797', 1, 'score', 1597126201955, 1, '比邻云盘 - VIP会员月度', 100, 1);
INSERT INTO `myl_orders` VALUES (19, '2021-11-26 11:20:12', '2021-11-26 11:20:12', NULL, 1, '20211126112011849290', 2, 'payjs', 0, 1, '比邻云盘 - 1 积分', 10, 0);
INSERT INTO `myl_orders` VALUES (20, '2021-11-26 11:20:15', '2021-11-26 11:20:15', NULL, 1, '20211126112014217018', 2, 'alipay', 0, 1, '比邻云盘 - 1 积分', 10, 0);
INSERT INTO `myl_orders` VALUES (21, '2021-11-26 11:20:26', '2021-11-26 11:20:26', NULL, 1, '20211126112026100079', 1, 'score', 1597126201955, 1, '比邻云盘 - VIP会员月度', 100, 1);
INSERT INTO `myl_orders` VALUES (22, '2022-05-05 18:57:21', '2022-05-05 18:57:21', NULL, 1, '20220505185721449077', 0, 'score', 1597127237595, 1, '比邻云盘 - 100GB扩容包(联系客服免费扩容)', 100, 1);
INSERT INTO `myl_orders` VALUES (23, '2022-05-05 18:57:46', '2022-05-05 18:57:46', NULL, 9, '20220505185746420643', 0, 'score', 1597127237595, 1, '比邻云盘 - 100GB扩容包(联系客服免费扩容)', 100, 1);

-- ----------------------------
-- Table structure for myl_policies
-- ----------------------------
DROP TABLE IF EXISTS `myl_policies`;
CREATE TABLE `myl_policies`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `bucket_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_private` tinyint(1) NULL DEFAULT NULL,
  `base_url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `access_key` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `secret_key` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `max_size` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `auto_rename` tinyint(1) NULL DEFAULT NULL,
  `dir_name_rule` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `file_name_rule` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_origin_link_enable` tinyint(1) NULL DEFAULT NULL,
  `options` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_myl_policies_deleted_at`(`deleted_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_policies
-- ----------------------------
INSERT INTO `myl_policies` VALUES (1, '2020-08-10 19:58:45', '2020-08-10 20:30:41', NULL, '默认存储策略', 'local', '', '', 0, '', '', '', 0, 1, 'uploads/{uid}/{path}', '{uid}_{randomkey8}_{originname}', 1, '{\"token\":\"\",\"file_type\":[],\"mimetype\":\"\"}');
INSERT INTO `myl_policies` VALUES (2, '2020-08-10 20:14:28', '2021-11-29 10:14:35', NULL, '无限存储User01区', 'onedrive', 'https://microsoftgraph.chinacloudapi.cn/v1.0', 'f806a8e1-5d9c-48cc-b8aa-f75443092237', 1, 'https://login.chinacloudapi.cn/common/oauth2', '0.AAAAIMoOfFuhLEGFEOJzHv5TMeGoBvicXcxIuKr3VEMJIjcBAGg.AgABAAEAAADF8uhbdqMKTqrrFtoyDhmYAwDs_wQA8_-z0lSGj0sLhKzPn7U7Pjvy5MCL0ejcPdqezen9f6juM3fFcDZhUXoNZAs4jDCppom3zeTk-hLRm9KS2t8Ld-dNw5cRx-QK7Lfk1EFuKzjxI5EFmNAvRhgiEPbzCpFBHkTfw_T5LwrjtgCB85nCLCxpaCfyS_YyrlqlpQRdA1QX5AZp7npUYWFz5_z3Ef3ydkXqIC9g4Y6rYSbqJGCpMGhTxct59zobTqz_eUak0HNDoWJv2oWi1A9uuxLIjBWrq1EI95IMm1u5dRG5wcwscYWcEHL-rGyHSXWkDe8r0y5Ir6O4am3PIlpOfXkv6Xmj3kvwCEWmVIlsh8smRG979kgAt74GE1T0GHxXnGkdJp8EdAZZfV0-YmwXxbeyQd9PDArvHhhZN6NB-ht-Wag1n5B6M-HSXS5RwN6rXyWIiGNPUscwlQDxO89Nk78hFHXzDAK8JqwtPAQKl1aUnCjgQP5f2t3hlnDfKicTzH5wKItk9fcPeifLRdEmUpPSUC70qOSRK4bbut3bdZdUcSQTRvZY0D4xaA0tev4hTMcfB4pXknB2wuLh8ZhgjlCI84SuTnMplYiOIL52tQMfXrP1RAD7rPJZWiZD28zWDmlIqIBYmSHs4oGpg193dFxeDFzfgm0uRZ_GhhVHBrS3aPcOcuFYCwJ2Ou22G2LF6dKhzg0wneQQoRS4KhvZ70mHqRsy3C7cJDbAgRZCfCmPF6C4i74PBa_xjBbv', 'aT_7CAmuXIdfU0_eE.c5P0N~BUbU_FRh4Y', 104857600, 0, 'uploads/{uid}/{path}', '{randomkey8}_{originname}', 1, '{\"token\":\"\",\"file_type\":[],\"mimetype\":\"\",\"od_redirect\":\"https://pan.bilnn.com/api/v3/callback/onedrive/auth\"}');
INSERT INTO `myl_policies` VALUES (3, '2020-08-11 13:54:52', '2020-08-11 13:56:11', NULL, '无限存储VIP01区', 'onedrive', 'https://microsoftgraph.chinacloudapi.cn/v1.0', 'f806a8e1-5d9c-48cc-b8aa-f75443092237', 1, 'https://login.chinacloudapi.cn/common/oauth2', 'OAQABAAAAAAAs2RXPF2swRJpJRjRbIf8x1YwBex2v-u-uquH9KPMsldB9H8U0qKnnJSNVMdJVsAUJTI17TezNpjstmhT9ZKqAMQSFhQVPv2mwEtI-dKuFMTaOzs20fa5lM0SH2NcDH_hTrzD4WTwYfjp7Vrce129nNrpFM9f5-HUYdLFKH9XtWpc9MvJe3ywYPsTSz2SO_heC28-aST7rAO2DRSsV149Iye-7xgPMGFZkESDcY-I-R4aH4VCcozcM9CWIvyBm86Yp4l24XzytMNqdfPWr4GcF7PTvw49QfuKLD04Y_wsqK6fKPp7Rwr_JKOEVC5TXBsTttouGpYQ8E_w54rjj6cukg1QMkMZ_LdjAi8vJK4rGJuPkF8Dln5P6yDkinlcXuoN4U_YbNOn4pxMyNgWtez5XaUsGD611Koq3yOi5V6-UwpQQakmfbiOikhTxFCyxAdovvdvcvrR47JJkipoKtEXkgBeS9GaKcti1RM0CC06w8U2E6cfne0UKcECQKwWKQtY8TyLWtVrC79k2lQyTU3F7Cd5NIBR4D-q38vudLOa3ccxmBXH2XRsU64kA5yc0zRFG68nlSKYEcbAr7sTfseML6SmzpUPtU7Ng8LibPx5Q1lHIE_zTVxsBWPCE9i3fZh6o1gzdIunurRJA7MZ3P6Dq9qkSGx4ZGJQ_xuZKuBbBx-Y2BvJbD85N9HZnEWCm6a7lJWwDsVEpHpmaAUgDxdA05Ptl23j0L_oNHxIbuXKKrwaq27v0lpWy4vF7KkJnaonTNF-fy8BgE3bC2jJIKpsVaGvaYjQJPjt1WXDmF7pQGYeRbocuXtD4DiOqMq8U46luspHjV_EynjWSuIg_22v3qEZmc0H9LjpRrSz3whvYRdWy_jtCLvCH3sAp9DAg3FlWN0zvq2GOZcYxUBGJJpgamqNyF6eMWOvphLnO07pWlNbtYy-svBRSPNjQVy3YF6ggAA', 'aT_7CAmuXIdfU0_eE.c5P0N~BUbU_FRh4Y', 1073741824, 0, 'uploads/{uid}/{path}', '{randomkey8}_{originname}', 1, '{\"token\":\"\",\"file_type\":[],\"mimetype\":\"\",\"od_redirect\":\"https://pan.bilnn.com/api/v3/callback/onedrive/auth\"}');

-- ----------------------------
-- Table structure for myl_redeems
-- ----------------------------
DROP TABLE IF EXISTS `myl_redeems`;
CREATE TABLE `myl_redeems`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `product_id` bigint(20) NULL DEFAULT NULL,
  `num` int(11) NULL DEFAULT NULL,
  `code` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `used` tinyint(1) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_myl_redeems_deleted_at`(`deleted_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_redeems
-- ----------------------------
INSERT INTO `myl_redeems` VALUES (1, '2020-08-11 14:18:07', '2020-08-11 14:18:16', NULL, 2, 0, 1, 'e5f1830e-e5ea-4f94-87a0-3f3018065deb', 1);
INSERT INTO `myl_redeems` VALUES (2, '2020-08-18 17:23:03', '2020-08-18 17:23:18', NULL, 2, 0, 100, '3a37d903-b14d-4401-ac7c-f6a8027ab30a', 1);

-- ----------------------------
-- Table structure for myl_reports
-- ----------------------------
DROP TABLE IF EXISTS `myl_reports`;
CREATE TABLE `myl_reports`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `share_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `reason` int(11) NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_myl_reports_deleted_at`(`deleted_at`) USING BTREE,
  INDEX `share_id`(`share_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_reports
-- ----------------------------

-- ----------------------------
-- Table structure for myl_settings
-- ----------------------------
DROP TABLE IF EXISTS `myl_settings`;
CREATE TABLE `myl_settings`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` longtext CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE,
  INDEX `idx_myl_settings_deleted_at`(`deleted_at`) USING BTREE,
  INDEX `setting_key`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 525 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_settings
-- ----------------------------
INSERT INTO `myl_settings` VALUES (1, '2020-08-10 19:58:45', '2022-05-05 15:44:58', NULL, 'basic', 'siteURL', 'https://pan.bilnn.com');
INSERT INTO `myl_settings` VALUES (2, '2020-08-10 19:58:45', '2022-05-05 15:44:58', NULL, 'basic', 'siteName', '比邻云盘');
INSERT INTO `myl_settings` VALUES (3, '2020-08-10 19:58:45', '2022-05-05 15:44:58', NULL, 'basic', 'siteICPId', '鲁ICP备18045286号-1');
INSERT INTO `myl_settings` VALUES (4, '2020-08-10 19:58:45', '2023-01-03 09:09:25', NULL, 'register', 'register_enabled', '1');
INSERT INTO `myl_settings` VALUES (5, '2020-08-10 19:58:45', '2023-01-03 09:09:25', NULL, 'register', 'default_group', '2');
INSERT INTO `myl_settings` VALUES (6, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'basic', 'siteKeywords', '网盘，网盘');
INSERT INTO `myl_settings` VALUES (7, '2020-08-10 19:58:45', '2022-05-05 15:44:58', NULL, 'basic', 'siteDes', '比邻云盘无限云存储直链下载,永久免费的无限流量无限存储空间不限速技术下载服务, 您可以通过比邻云方便地上传分享文件。 可以让您的资源、软件、应用等各种文件，随时随地触手可及，更好的分享。');
INSERT INTO `myl_settings` VALUES (8, '2020-08-10 19:58:45', '2022-05-05 15:44:58', NULL, 'basic', 'siteTitle', '无限云存储直链下载');
INSERT INTO `myl_settings` VALUES (9, '2020-08-10 19:58:45', '2022-05-05 15:44:58', NULL, 'basic', 'siteScript', '');
INSERT INTO `myl_settings` VALUES (10, '2020-08-10 19:58:45', '2023-01-03 09:09:41', NULL, 'mail', 'fromName', '比邻云');
INSERT INTO `myl_settings` VALUES (11, '2020-08-10 19:58:45', '2023-01-03 09:09:41', NULL, 'mail', 'mail_keepalive', '30');
INSERT INTO `myl_settings` VALUES (12, '2020-08-10 19:58:45', '2023-01-03 09:09:41', NULL, 'mail', 'fromAdress', 'cloud@bilnn.com');
INSERT INTO `myl_settings` VALUES (13, '2020-08-10 19:58:45', '2023-01-03 09:09:41', NULL, 'mail', 'smtpHost', 'smtp.ym.163.com');
INSERT INTO `myl_settings` VALUES (14, '2020-08-10 19:58:45', '2023-01-03 09:09:41', NULL, 'mail', 'smtpPort', '994');
INSERT INTO `myl_settings` VALUES (15, '2020-08-10 19:58:45', '2023-01-03 09:09:41', NULL, 'mail', 'replyTo', 'cloud@bilnn.com');
INSERT INTO `myl_settings` VALUES (16, '2020-08-10 19:58:45', '2023-01-03 09:09:41', NULL, 'mail', 'smtpUser', 'cloud@bilnn.com');
INSERT INTO `myl_settings` VALUES (17, '2020-08-10 19:58:45', '2023-01-03 09:09:41', NULL, 'mail', 'smtpPass', '1233');
INSERT INTO `myl_settings` VALUES (18, '2020-08-10 19:58:45', '2023-01-03 09:09:41', NULL, 'mail', 'smtpEncryption', '1');
INSERT INTO `myl_settings` VALUES (19, '2020-08-10 19:58:45', '2023-01-03 09:09:41', NULL, 'mail_template', 'over_used_template', '<meta name=\"viewport\"content=\"width=device-width\"><meta http-equiv=\"Content-Type\"content=\"text/html; charset=UTF-8\"><title>容量超额提醒</title><style type=\"text/css\">img{max-width:100%}body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%!important;height:100%;line-height:1.6em}body{background-color:#f6f6f6}@media only screen and(max-width:640px){body{padding:0!important}h1{font-weight:800!important;margin:20px 0 5px!important}h2{font-weight:800!important;margin:20px 0 5px!important}h3{font-weight:800!important;margin:20px 0 5px!important}h4{font-weight:800!important;margin:20px 0 5px!important}h1{font-size:22px!important}h2{font-size:18px!important}h3{font-size:16px!important}.container{padding:0!important;width:100%!important}.content{padding:0!important}.content-wrap{padding:10px!important}.invoice{width:100%!important}}</style><table class=\"body-wrap\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;\"bgcolor=\"#f6f6f6\"><tbody><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;\"valign=\"top\"></td><td class=\"container\"width=\"600\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;\"valign=\"top\"><div class=\"content\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;\"><table class=\"main\"width=\"100%\"cellpadding=\"0\"cellspacing=\"0\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px \nsolid #e9e9e9;\"bgcolor=\"#fff\"><tbody><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"alert alert-warning\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #FF9F00; margin: 0; padding: 20px;\"align=\"center\"bgcolor=\"#FF9F00\"valign=\"top\">容量超额警告</td></tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-wrap\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\"valign=\"top\"><table width=\"100%\"cellpadding=\"0\"cellspacing=\"0\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><tbody><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\"valign=\"top\">亲爱的<strong style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\">{userName}</strong>：</td></tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\"valign=\"top\">由于{notifyReason}，您在{siteTitle}的账户的容量使用超出配额，您将无法继续上传新文件，请尽快清理文件，否则我们将会禁用您的账户。</td></tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\"valign=\"top\"><a href=\"{siteUrl}Login\"class=\"btn-primary\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #348eda; margin: 0; border-color: #348eda; border-style: solid; border-width: 10px 20px;\">登录{siteTitle}</a></td></tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\"valign=\"top\">感谢您选择{siteTitle}。</td></tr></tbody></table></td></tr></tbody></table><div class=\"footer\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\"><table width=\"100%\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><tbody><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"aligncenter content-block\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;\"align=\"center\"valign=\"top\">此邮件由系统自动发送，请不要直接回复。</td></tr></tbody></table></div></div></td><td style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;\"valign=\"top\"></td></tr></tbody></table>');
INSERT INTO `myl_settings` VALUES (20, '2020-08-10 19:58:45', '2023-01-03 09:10:12', NULL, 'storage_policy', 'ban_time', '604800');
INSERT INTO `myl_settings` VALUES (21, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'file_edit', 'maxEditSize', '4194304');
INSERT INTO `myl_settings` VALUES (22, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'timeout', 'archive_timeout', '60');
INSERT INTO `myl_settings` VALUES (23, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'timeout', 'download_timeout', '60');
INSERT INTO `myl_settings` VALUES (24, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'timeout', 'preview_timeout', '60');
INSERT INTO `myl_settings` VALUES (25, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'timeout', 'doc_preview_timeout', '60');
INSERT INTO `myl_settings` VALUES (26, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'timeout', 'upload_credential_timeout', '1800');
INSERT INTO `myl_settings` VALUES (27, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'timeout', 'upload_session_timeout', '86400');
INSERT INTO `myl_settings` VALUES (28, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'timeout', 'slave_api_timeout', '60');
INSERT INTO `myl_settings` VALUES (29, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'timeout', 'onedrive_monitor_timeout', '600');
INSERT INTO `myl_settings` VALUES (30, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'timeout', 'onedrive_source_timeout', '1800');
INSERT INTO `myl_settings` VALUES (31, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'timeout', 'share_download_session_timeout', '2073600');
INSERT INTO `myl_settings` VALUES (32, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'timeout', 'onedrive_callback_check', '20');
INSERT INTO `myl_settings` VALUES (33, '2020-08-10 19:58:45', '2020-08-11 12:39:11', NULL, 'timeout', 'aria2_call_timeout', '5');
INSERT INTO `myl_settings` VALUES (34, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'retry', 'onedrive_chunk_retries', '1');
INSERT INTO `myl_settings` VALUES (35, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'upload', 'reset_after_upload_failed', '0');
INSERT INTO `myl_settings` VALUES (36, '2020-08-10 19:58:45', '2023-01-03 09:09:25', NULL, 'login', 'login_captcha', '0');
INSERT INTO `myl_settings` VALUES (37, '2020-08-10 19:58:45', '2023-01-03 09:09:25', NULL, 'login', 'qq_login', '1');
INSERT INTO `myl_settings` VALUES (38, '2020-08-10 19:58:45', '2023-01-03 09:09:25', NULL, 'login', 'qq_direct_login', '1');
INSERT INTO `myl_settings` VALUES (39, '2020-08-10 19:58:45', '2023-01-03 09:09:25', NULL, 'login', 'qq_login_id', '123');
INSERT INTO `myl_settings` VALUES (40, '2020-08-10 19:58:45', '2023-01-03 09:09:25', NULL, 'login', 'qq_login_key', '123');
INSERT INTO `myl_settings` VALUES (41, '2020-08-10 19:58:45', '2023-01-03 09:09:25', NULL, 'login', 'reg_captcha', '0');
INSERT INTO `myl_settings` VALUES (42, '2020-08-10 19:58:45', '2023-01-03 09:09:25', NULL, 'register', 'email_active', '0');
INSERT INTO `myl_settings` VALUES (43, '2020-08-10 19:58:45', '2023-01-03 09:09:41', NULL, 'mail_template', 'mail_activation_template', '<!DOCTYPE html PUBLIC\"-//W3C//DTD XHTML 1.0 Transitional//EN\"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; \nfont-size: 14px; margin: 0;\"><head><meta name=\"viewport\"content=\"width=device-width\"/><meta http-equiv=\"Content-Type\"content=\"text/html; charset=UTF-8\"/><title>用户激活</title><style type=\"text/css\">img{max-width:100%}body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%!important;height:100%;line-height:1.6em}body{background-color:#f6f6f6}@media only screen and(max-width:640px){body{padding:0!important}h1{font-weight:800!important;margin:20px 0 5px!important}h2{font-weight:800!important;margin:20px 0 5px!important}h3{font-weight:800!important;margin:20px 0 5px!important}h4{font-weight:800!important;margin:20px 0 5px!important}h1{font-size:22px!important}h2{font-size:18px!important}h3{font-size:16px!important}.container{padding:0!important;width:100%!important}.content{padding:0!important}.content-wrap{padding:10px!important}.invoice{width:100%!important}}</style></head><body itemscope itemtype=\"http://schema.org/EmailMessage\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: \nborder-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;\"bgcolor=\"#f6f6f6\"><table class=\"body-wrap\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;\"bgcolor=\"#f6f6f6\"><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; \nbox-sizing: border-box; font-size: 14px; margin: 0;\"><td style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;\"valign=\"top\"></td><td class=\"container\"width=\"600\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;\"valign=\"top\"><div class=\"content\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;\"><table class=\"main\"width=\"100%\"cellpadding=\"0\"cellspacing=\"0\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px \nsolid #e9e9e9;\"bgcolor=\"#fff\"><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: \n14px; margin: 0;\"><td class=\"alert alert-warning\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #009688; margin: 0; padding: 20px;\"align=\"center\"bgcolor=\"#FF9F00\"valign=\"top\">激活{siteTitle}账户</td></tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-wrap\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\"valign=\"top\"><table width=\"100%\"cellpadding=\"0\"cellspacing=\"0\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\"style=\"font-family: \'Helvetica \nNeue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\"valign=\"top\">亲爱的<strong style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\">{userName}</strong>：</td></tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\"valign=\"top\">感谢您注册{siteTitle},请点击下方按钮完成账户激活。</td></tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\"valign=\"top\"><a href=\"{activationUrl}\"class=\"btn-primary\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #009688; margin: 0; border-color: #009688; border-style: solid; border-width: 10px 20px;\">激活账户</a></td></tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\"valign=\"top\">感谢您选择{siteTitle}。</td></tr></table></td></tr></table><div class=\"footer\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\"><table width=\"100%\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"aligncenter content-block\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;\"align=\"center\"valign=\"top\">此邮件由系统自动发送，请不要直接回复。</td></tr></table></div></div></td><td style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;\"valign=\"top\"></td></tr></table></body></html>');
INSERT INTO `myl_settings` VALUES (44, '2020-08-10 19:58:45', '2023-01-03 09:09:25', NULL, 'login', 'forget_captcha', '0');
INSERT INTO `myl_settings` VALUES (45, '2020-08-10 19:58:45', '2023-01-03 09:09:41', NULL, 'mail_template', 'mail_reset_pwd_template', '<!DOCTYPE html PUBLIC\"-//W3C//DTD XHTML 1.0 Transitional//EN\"\"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\"><html xmlns=\"http://www.w3.org/1999/xhtml\"style=\"font-family: \'Helvetica Neue\', Helvetica, Arial, sans-serif; box-sizing: border-box; \nfont-size: 14px; margin: 0;\"><head><meta name=\"viewport\"content=\"width=device-width\"/><meta http-equiv=\"Content-Type\"content=\"text/html; charset=UTF-8\"/><title>重设密码</title><style type=\"text/css\">img{max-width:100%}body{-webkit-font-smoothing:antialiased;-webkit-text-size-adjust:none;width:100%!important;height:100%;line-height:1.6em}body{background-color:#f6f6f6}@media only screen and(max-width:640px){body{padding:0!important}h1{font-weight:800!important;margin:20px 0 5px!important}h2{font-weight:800!important;margin:20px 0 5px!important}h3{font-weight:800!important;margin:20px 0 5px!important}h4{font-weight:800!important;margin:20px 0 5px!important}h1{font-size:22px!important}h2{font-size:18px!important}h3{font-size:16px!important}.container{padding:0!important;width:100%!important}.content{padding:0!important}.content-wrap{padding:10px!important}.invoice{width:100%!important}}</style></head><body itemscope itemtype=\"http://schema.org/EmailMessage\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: \nborder-box; font-size: 14px; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; line-height: 1.6em; background-color: #f6f6f6; margin: 0;\"bgcolor=\"#f6f6f6\"><table class=\"body-wrap\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; background-color: #f6f6f6; margin: 0;\"bgcolor=\"#f6f6f6\"><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; \nbox-sizing: border-box; font-size: 14px; margin: 0;\"><td style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;\"valign=\"top\"></td><td class=\"container\"width=\"600\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto;\"valign=\"top\"><div class=\"content\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; max-width: 600px; display: block; margin: 0 auto; padding: 20px;\"><table class=\"main\"width=\"100%\"cellpadding=\"0\"cellspacing=\"0\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; border-radius: 3px; background-color: #fff; margin: 0; border: 1px \nsolid #e9e9e9;\"bgcolor=\"#fff\"><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: \n14px; margin: 0;\"><td class=\"alert alert-warning\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 16px; vertical-align: top; color: #fff; font-weight: 500; text-align: center; border-radius: 3px 3px 0 0; background-color: #2196F3; margin: 0; padding: 20px;\"align=\"center\"bgcolor=\"#FF9F00\"valign=\"top\">重设{siteTitle}密码</td></tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-wrap\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 20px;\"valign=\"top\"><table width=\"100%\"cellpadding=\"0\"cellspacing=\"0\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\"style=\"font-family: \'Helvetica \nNeue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\"valign=\"top\">亲爱的<strong style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\">{userName}</strong>：</td></tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\"valign=\"top\">请点击下方按钮完成密码重设。如果非你本人操作，请忽略此邮件。</td></tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\"valign=\"top\"><a href=\"{resetUrl}\"class=\"btn-primary\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; color: #FFF; text-decoration: none; line-height: 2em; font-weight: bold; text-align: center; cursor: pointer; display: inline-block; border-radius: 5px; text-transform: capitalize; background-color: #2196F3; margin: 0; border-color: #2196F3; border-style: solid; border-width: 10px 20px;\">重设密码</a></td></tr><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"content-block\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0; padding: 0 0 20px;\"valign=\"top\">感谢您选择{siteTitle}。</td></tr></table></td></tr></table><div class=\"footer\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; width: 100%; clear: both; color: #999; margin: 0; padding: 20px;\"><table width=\"100%\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><tr style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; margin: 0;\"><td class=\"aligncenter content-block\"style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 12px; vertical-align: top; color: #999; text-align: center; margin: 0; padding: 0 0 20px;\"align=\"center\"valign=\"top\">此邮件由系统自动发送，请不要直接回复。</td></tr></table></div></div></td><td style=\"font-family: \'Helvetica Neue\',Helvetica,Arial,sans-serif; box-sizing: border-box; font-size: 14px; vertical-align: top; margin: 0;\"valign=\"top\"></td></tr></table></body></html>');
INSERT INTO `myl_settings` VALUES (46, '2020-08-10 19:58:45', '2023-01-03 09:10:12', NULL, 'pack', 'pack_data', '[{\"name\":\"10GB扩容包(联系客服免费扩容)\",\"size\":10737418240,\"time\":31536000,\"price\":100,\"score\":10,\"id\":1597127200064},{\"name\":\"100GB扩容包(联系客服免费扩容)\",\"size\":107374182400,\"time\":31536000,\"price\":1000,\"score\":100,\"id\":1597127237595},{\"name\":\"1TB扩容包(联系客服免费扩容)\",\"size\":1099511627776,\"time\":31536000,\"price\":10000,\"score\":1000,\"id\":1597127262962},{\"name\":\"2TB扩容包(联系客服免费扩容)\",\"size\":10995116277760,\"time\":31536000,\"price\":15000,\"score\":1500,\"id\":1597127328232}]');
INSERT INTO `myl_settings` VALUES (47, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'version', 'db_version_3.1.0', 'installed');
INSERT INTO `myl_settings` VALUES (48, '2020-08-10 19:58:45', '2023-01-03 09:10:12', NULL, 'payment', 'alipay_enabled', '1');
INSERT INTO `myl_settings` VALUES (49, '2020-08-10 19:58:45', '2023-01-03 09:10:12', NULL, 'payment', 'payjs_enabled', '1');
INSERT INTO `myl_settings` VALUES (50, '2020-08-10 19:58:45', '2023-01-03 09:10:12', NULL, 'payment', 'payjs_id', '123');
INSERT INTO `myl_settings` VALUES (51, '2020-08-10 19:58:45', '2023-01-03 09:10:12', NULL, 'payment', 'payjs_secret', '123');
INSERT INTO `myl_settings` VALUES (52, '2020-08-10 19:58:45', '2023-01-03 09:10:12', NULL, 'payment', 'appid', '123');
INSERT INTO `myl_settings` VALUES (53, '2020-08-10 19:58:45', '2023-01-03 09:10:12', NULL, 'payment', 'appkey', '123');
INSERT INTO `myl_settings` VALUES (54, '2020-08-10 19:58:45', '2023-01-03 09:10:12', NULL, 'payment', 'shopid', '123');
INSERT INTO `myl_settings` VALUES (55, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'share', 'hot_share_num', '10');
INSERT INTO `myl_settings` VALUES (56, '2020-08-10 19:58:45', '2023-01-03 09:10:12', NULL, 'group_sell', 'group_sell_data', '[{\"name\":\"VIP会员月度\",\"group_id\":4,\"time\":2592000,\"price\":1000,\"score\":100,\"des\":[\"单文件上传支持1G\",\"分享文件免积分下载\",\"http或者磁力链离线下载\",\"在线打包下载/在线解压缩\",\"WebDAV、API、SDK集成和技术支持\"],\"highlight\":true,\"id\":1597126201955},{\"name\":\"VIP会员季度\",\"group_id\":4,\"time\":7776000,\"price\":2500,\"score\":250,\"des\":[\"单文件上传支持1G\",\"分享文件免积分下载\",\"http或者磁力链离线下载\",\"在线打包下载/在线解压缩\",\"WebDAV、API、SDK集成和技术支持\"],\"highlight\":false,\"id\":1597126248456},{\"name\":\"VIP会员年度\",\"group_id\":4,\"time\":31104000,\"price\":10000,\"score\":1000,\"des\":[\"单文件上传支持1G\",\"分享文件免积分下载\",\"http或者磁力链离线下载\",\"在线打包下载/在线解压缩\",\"WebDAV、API、SDK集成和技术支持\"],\"highlight\":false,\"id\":1597126402688}]');
INSERT INTO `myl_settings` VALUES (57, '2020-08-10 19:58:45', '2022-05-05 15:52:14', NULL, 'avatar', 'gravatar_server', 'https://gravatar.loli.net/');
INSERT INTO `myl_settings` VALUES (58, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'basic', 'defaultTheme', '#3f51b5');
INSERT INTO `myl_settings` VALUES (59, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'basic', 'themes', '{\"#3f51b5\":{\"palette\":{\"primary\":{\"main\":\"#3f51b5\"},\"secondary\":{\"main\":\"#f50057\"}}},\"#2196f3\":{\"palette\":{\"primary\":{\"main\":\"#2196f3\"},\"secondary\":{\"main\":\"#FFC107\"}}},\"#673AB7\":{\"palette\":{\"primary\":{\"main\":\"#673AB7\"},\"secondary\":{\"main\":\"#2196F3\"}}},\"#E91E63\":{\"palette\":{\"primary\":{\"main\":\"#E91E63\"},\"secondary\":{\"main\":\"#42A5F5\",\"contrastText\":\"#fff\"}}},\"#FF5722\":{\"palette\":{\"primary\":{\"main\":\"#FF5722\"},\"secondary\":{\"main\":\"#3F51B5\"}}},\"#FFC107\":{\"palette\":{\"primary\":{\"main\":\"#FFC107\"},\"secondary\":{\"main\":\"#26C6DA\"}}},\"#8BC34A\":{\"palette\":{\"primary\":{\"main\":\"#8BC34A\",\"contrastText\":\"#fff\"},\"secondary\":{\"main\":\"#FF8A65\",\"contrastText\":\"#fff\"}}},\"#009688\":{\"palette\":{\"primary\":{\"main\":\"#009688\"},\"secondary\":{\"main\":\"#4DD0E1\",\"contrastText\":\"#fff\"}}},\"#607D8B\":{\"palette\":{\"primary\":{\"main\":\"#607D8B\"},\"secondary\":{\"main\":\"#F06292\"}}},\"#795548\":{\"palette\":{\"primary\":{\"main\":\"#795548\"},\"secondary\":{\"main\":\"#4CAF50\",\"contrastText\":\"#fff\"}}}}');
INSERT INTO `myl_settings` VALUES (60, '2020-08-10 19:58:45', '2020-08-11 12:39:11', '2021-11-26 11:11:01', 'aria2', 'aria2_token', 'a8387f118295e3433109');
INSERT INTO `myl_settings` VALUES (61, '2020-08-10 19:58:45', '2020-08-11 12:39:11', '2021-11-26 11:11:01', 'aria2', 'aria2_rpcurl', 'http://127.0.0.1:6800');
INSERT INTO `myl_settings` VALUES (62, '2020-08-10 19:58:45', '2020-08-11 12:39:11', '2021-11-26 11:11:01', 'aria2', 'aria2_temp_path', '/data/www/pan/temp/');
INSERT INTO `myl_settings` VALUES (63, '2020-08-10 19:58:45', '2020-08-11 12:39:11', '2021-11-26 11:11:01', 'aria2', 'aria2_options', '{}');
INSERT INTO `myl_settings` VALUES (64, '2020-08-10 19:58:45', '2020-08-11 12:39:11', '2021-11-26 11:11:01', 'aria2', 'aria2_interval', '60');
INSERT INTO `myl_settings` VALUES (65, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'task', 'max_worker_num', '10');
INSERT INTO `myl_settings` VALUES (66, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'task', 'max_parallel_transfer', '4');
INSERT INTO `myl_settings` VALUES (67, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'auth', 'secret_key', '6XjFRrdEG04oHDEyDN0IuoT1vyp6eXI2bpxf79nCo17MnKV9ToSWT8GTUZZv6GfU8j7J6ezuO7ljD8l8dfKPBHscEGN9JvicVUhyOzA9GQvAAjPU3eujkn8nuFBHd5wRSsHeUxxlkMLkucL7G35ADFIyiuzde2NWvGFlgfkSb04QfSfMpi4jadilnnAWc2UKTAGDBuMDRKitaCxf7OnFBeFY9haysjziDi86uosDPoEVwXo7QO0NLLg8IFk04NZx');
INSERT INTO `myl_settings` VALUES (68, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'path', 'temp_path', 'temp');
INSERT INTO `myl_settings` VALUES (69, '2020-08-10 19:58:45', '2022-05-05 15:52:14', NULL, 'path', 'avatar_path', 'avatar');
INSERT INTO `myl_settings` VALUES (70, '2020-08-10 19:58:45', '2022-05-05 15:52:14', NULL, 'avatar', 'avatar_size', '2097152');
INSERT INTO `myl_settings` VALUES (71, '2020-08-10 19:58:45', '2022-05-05 15:52:14', NULL, 'avatar', 'avatar_size_l', '200');
INSERT INTO `myl_settings` VALUES (72, '2020-08-10 19:58:45', '2022-05-05 15:52:14', NULL, 'avatar', 'avatar_size_m', '130');
INSERT INTO `myl_settings` VALUES (73, '2020-08-10 19:58:45', '2022-05-05 15:52:14', NULL, 'avatar', 'avatar_size_s', '50');
INSERT INTO `myl_settings` VALUES (74, '2020-08-10 19:58:45', '2023-01-03 09:10:12', NULL, 'score', 'score_enabled', '1');
INSERT INTO `myl_settings` VALUES (75, '2020-08-10 19:58:45', '2023-01-03 09:10:12', NULL, 'score', 'share_score_rate', '90');
INSERT INTO `myl_settings` VALUES (76, '2020-08-10 19:58:45', '2023-01-03 09:10:12', NULL, 'score', 'score_price', '10');
INSERT INTO `myl_settings` VALUES (77, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'view', 'home_view_method', 'icon');
INSERT INTO `myl_settings` VALUES (78, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'view', 'share_view_method', 'list');
INSERT INTO `myl_settings` VALUES (79, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'cron', 'cron_garbage_collect', '@hourly');
INSERT INTO `myl_settings` VALUES (80, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'cron', 'cron_notify_user', '@hourly');
INSERT INTO `myl_settings` VALUES (81, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'cron', 'cron_ban_user', '@hourly');
INSERT INTO `myl_settings` VALUES (82, '2020-08-10 19:58:45', '2023-01-03 09:09:25', NULL, 'authn', 'authn_enabled', '1');
INSERT INTO `myl_settings` VALUES (83, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'captcha', 'captcha_height', '60');
INSERT INTO `myl_settings` VALUES (84, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'captcha', 'captcha_width', '240');
INSERT INTO `myl_settings` VALUES (85, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'captcha', 'captcha_mode', '3');
INSERT INTO `myl_settings` VALUES (86, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'captcha', 'captcha_ComplexOfNoiseText', '0');
INSERT INTO `myl_settings` VALUES (87, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'captcha', 'captcha_ComplexOfNoiseDot', '0');
INSERT INTO `myl_settings` VALUES (88, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'captcha', 'captcha_IsShowHollowLine', '0');
INSERT INTO `myl_settings` VALUES (89, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'captcha', 'captcha_IsShowNoiseDot', '1');
INSERT INTO `myl_settings` VALUES (90, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'captcha', 'captcha_IsShowNoiseText', '0');
INSERT INTO `myl_settings` VALUES (91, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'captcha', 'captcha_IsShowSlimeLine', '1');
INSERT INTO `myl_settings` VALUES (92, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'captcha', 'captcha_IsShowSineLine', '0');
INSERT INTO `myl_settings` VALUES (93, '2020-08-10 19:58:45', '2020-08-10 19:58:45', NULL, 'captcha', 'captcha_CaptchaLen', '6');
INSERT INTO `myl_settings` VALUES (94, '2020-08-10 19:58:45', '2020-08-11 09:38:11', NULL, 'captcha', 'captcha_IsUseReCaptcha', '0');
INSERT INTO `myl_settings` VALUES (95, '2020-08-10 19:58:45', '2020-08-11 09:38:11', NULL, 'captcha', 'captcha_ReCaptchaKey', 'defaultKey');
INSERT INTO `myl_settings` VALUES (96, '2020-08-10 19:58:45', '2020-08-11 09:38:11', NULL, 'captcha', 'captcha_ReCaptchaSecret', 'defaultSecret');
INSERT INTO `myl_settings` VALUES (97, '2020-08-10 19:58:45', '2022-05-05 15:52:14', NULL, 'thumb', 'thumb_width', '400');
INSERT INTO `myl_settings` VALUES (98, '2020-08-10 19:58:45', '2022-05-05 15:52:14', NULL, 'thumb', 'thumb_height', '300');
INSERT INTO `myl_settings` VALUES (99, '2020-08-10 19:58:45', '2022-05-05 15:44:58', NULL, 'pwa', 'pwa_small_icon', '/static/img/favicon.ico');
INSERT INTO `myl_settings` VALUES (100, '2020-08-10 19:58:45', '2022-05-05 15:44:58', NULL, 'pwa', 'pwa_medium_icon', '/static/img/logo192.png');
INSERT INTO `myl_settings` VALUES (101, '2020-08-10 19:58:45', '2022-05-05 15:44:58', NULL, 'pwa', 'pwa_large_icon', '/static/img/logo512.png');
INSERT INTO `myl_settings` VALUES (102, '2020-08-10 19:58:45', '2022-05-05 15:44:58', NULL, 'pwa', 'pwa_display', 'standalone');
INSERT INTO `myl_settings` VALUES (103, '2020-08-10 19:58:45', '2022-05-05 15:44:58', NULL, 'pwa', 'pwa_theme_color', '#000000');
INSERT INTO `myl_settings` VALUES (104, '2020-08-10 19:58:45', '2022-05-05 15:44:58', NULL, 'pwa', 'pwa_background_color', '#ffffff');
INSERT INTO `myl_settings` VALUES (110, '2021-08-05 17:32:02', '2023-01-03 09:09:25', NULL, 'register', 'mail_domain_filter', '0');
INSERT INTO `myl_settings` VALUES (111, '2021-08-05 17:32:02', '2023-01-03 09:09:25', NULL, 'register', 'mail_domain_filter_list', '126.com,163.com,gmail.com,outlook.com,qq.com,foxmail.com,yeah.net,sohu.com,sohu.cn,139.com,wo.cn,189.cn,hotmail.com,live.com,live.cn');
INSERT INTO `myl_settings` VALUES (115, '2021-08-05 17:32:02', '2022-05-05 15:44:58', NULL, 'basic', 'siteNotice', '');
INSERT INTO `myl_settings` VALUES (141, '2021-08-05 17:32:02', '2021-08-05 17:32:02', NULL, 'timeout', 'folder_props_timeout', '300');
INSERT INTO `myl_settings` VALUES (155, '2021-08-05 17:32:02', '2021-08-05 17:32:02', NULL, 'version', 'db_version_3.3.2-pro', 'installed');
INSERT INTO `myl_settings` VALUES (163, '2021-08-05 17:32:02', '2023-01-03 09:10:12', NULL, 'payment', 'wechat_enabled', '0');
INSERT INTO `myl_settings` VALUES (164, '2021-08-05 17:32:02', '2023-01-03 09:10:12', NULL, 'payment', 'wechat_appid', '');
INSERT INTO `myl_settings` VALUES (165, '2021-08-05 17:32:02', '2023-01-03 09:10:12', NULL, 'payment', 'wechat_mchid', '');
INSERT INTO `myl_settings` VALUES (166, '2021-08-05 17:32:02', '2023-01-03 09:10:12', NULL, 'payment', 'wechat_serial_no', '');
INSERT INTO `myl_settings` VALUES (167, '2021-08-05 17:32:02', '2023-01-03 09:10:12', NULL, 'payment', 'wechat_api_key', '');
INSERT INTO `myl_settings` VALUES (168, '2021-08-05 17:32:02', '2023-01-03 09:10:12', NULL, 'payment', 'wechat_pk_content', '');
INSERT INTO `myl_settings` VALUES (197, '2021-08-05 17:32:02', '2021-08-05 17:32:02', NULL, 'captcha', 'captcha_type', 'normal');
INSERT INTO `myl_settings` VALUES (211, '2021-08-05 17:32:02', '2021-08-05 17:32:02', NULL, 'captcha', 'captcha_TCaptcha_CaptchaAppId', '');
INSERT INTO `myl_settings` VALUES (212, '2021-08-05 17:32:02', '2021-08-05 17:32:02', NULL, 'captcha', 'captcha_TCaptcha_AppSecretKey', '');
INSERT INTO `myl_settings` VALUES (213, '2021-08-05 17:32:02', '2021-08-05 17:32:02', NULL, 'captcha', 'captcha_TCaptcha_SecretId', '');
INSERT INTO `myl_settings` VALUES (214, '2021-08-05 17:32:02', '2021-08-05 17:32:02', NULL, 'captcha', 'captcha_TCaptcha_SecretKey', '');
INSERT INTO `myl_settings` VALUES (223, '2021-08-05 17:32:02', '2023-01-03 09:09:25', NULL, 'register', 'initial_files', '[]');
INSERT INTO `myl_settings` VALUES (236, '2021-11-26 11:11:01', '2021-11-26 11:11:01', NULL, 'basic', 'siteID', '26a7e39a-0fbc-419b-a0bc-62928b1d4542');
INSERT INTO `myl_settings` VALUES (256, '2021-11-26 11:11:01', '2021-11-26 11:11:01', NULL, 'slave', 'slave_node_retry', '3');
INSERT INTO `myl_settings` VALUES (257, '2021-11-26 11:11:01', '2021-11-26 11:11:01', NULL, 'slave', 'slave_ping_interval', '60');
INSERT INTO `myl_settings` VALUES (258, '2021-11-26 11:11:01', '2021-11-26 11:11:01', NULL, 'slave', 'slave_recover_interval', '120');
INSERT INTO `myl_settings` VALUES (259, '2021-11-26 11:11:01', '2021-11-26 11:11:01', NULL, 'timeout', 'slave_transfer_timeout', '172800');
INSERT INTO `myl_settings` VALUES (278, '2021-11-26 11:11:01', '2021-11-26 11:11:01', NULL, 'version', 'db_version_3.4.0-pro', 'installed');
INSERT INTO `myl_settings` VALUES (342, '2021-11-26 11:11:01', '2022-05-05 15:52:14', NULL, 'preview', 'office_preview_service', 'https://view.officeapps.live.com/op/view.aspx?src={$src}');
INSERT INTO `myl_settings` VALUES (383, '2022-04-20 16:45:06', '2022-04-20 16:45:06', NULL, 'retry', 'chunk_retries', '5');
INSERT INTO `myl_settings` VALUES (385, '2022-04-20 16:45:06', '2022-04-20 16:45:06', NULL, 'upload', 'use_temp_chunk_buffer', '1');
INSERT INTO `myl_settings` VALUES (397, '2022-04-20 16:45:06', '2022-04-20 16:45:06', NULL, 'version', 'db_version_3.5.0-beta1-pro', 'installed');
INSERT INTO `myl_settings` VALUES (433, '2022-04-20 16:45:06', '2022-04-20 16:45:06', NULL, 'cron', 'cron_recycle_upload_session', '@every 1h30m');
INSERT INTO `myl_settings` VALUES (455, '2022-04-20 16:45:06', '2022-05-05 15:52:14', NULL, 'thumb', 'thumb_file_suffix', '._thumb');
INSERT INTO `myl_settings` VALUES (456, '2022-04-20 16:45:06', '2022-05-05 15:52:14', NULL, 'thumb', 'thumb_max_task_count', '-1');
INSERT INTO `myl_settings` VALUES (457, '2022-04-20 16:45:06', '2022-05-05 15:52:14', NULL, 'thumb', 'thumb_encode_method', 'jpg');
INSERT INTO `myl_settings` VALUES (458, '2022-04-20 16:45:06', '2022-05-05 15:52:14', NULL, 'thumb', 'thumb_gc_after_gen', '0');
INSERT INTO `myl_settings` VALUES (459, '2022-04-20 16:45:06', '2022-05-05 15:52:14', NULL, 'thumb', 'thumb_encode_quality', '85');
INSERT INTO `myl_settings` VALUES (468, '2022-04-20 16:45:06', '2022-04-20 16:45:06', NULL, 'phone', 'phone_required', 'false');
INSERT INTO `myl_settings` VALUES (469, '2022-04-20 16:45:06', '2022-04-20 16:45:06', NULL, 'phone', 'phone_enabled', 'false');
INSERT INTO `myl_settings` VALUES (524, '2022-05-05 15:27:27', '2022-05-05 15:27:27', NULL, 'version', 'db_version_3.5.2-pro', 'installed');

-- ----------------------------
-- Table structure for myl_shares
-- ----------------------------
DROP TABLE IF EXISTS `myl_shares`;
CREATE TABLE `myl_shares`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_dir` tinyint(1) NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `source_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `views` int(11) NULL DEFAULT NULL,
  `downloads` int(11) NULL DEFAULT NULL,
  `remain_downloads` int(11) NULL DEFAULT NULL,
  `expires` timestamp NULL DEFAULT NULL,
  `score` int(11) NULL DEFAULT NULL,
  `preview_enabled` tinyint(1) NULL DEFAULT NULL,
  `source_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_myl_shares_deleted_at`(`deleted_at`) USING BTREE,
  INDEX `source`(`source_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 18 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_shares
-- ----------------------------
INSERT INTO `myl_shares` VALUES (1, '2020-08-10 20:28:50', '2020-08-10 20:28:50', NULL, '', 0, 1, 2, 3, 0, -1, NULL, 0, 1, 'key.bin');
INSERT INTO `myl_shares` VALUES (2, '2020-08-12 13:53:10', '2020-08-12 13:53:10', NULL, '', 0, 1, 2, 3, 0, -1, NULL, 0, 1, 'key.bin');
INSERT INTO `myl_shares` VALUES (3, '2020-08-12 14:15:07', '2020-08-12 14:15:07', NULL, '', 0, 1, 2, 4, 0, -1, NULL, 0, 1, 'key.bin');
INSERT INTO `myl_shares` VALUES (4, '2020-08-18 13:41:52', '2021-09-01 16:53:05', '2022-05-05 20:16:57', '', 0, 1, 5, 9, 1, -1, NULL, 0, 1, 'video.mp4');
INSERT INTO `myl_shares` VALUES (5, '2020-08-18 17:21:14', '2021-08-05 20:05:14', NULL, 'mrriqz', 0, 1, 2, 3, 1, -1, NULL, 100, 1, 'key.bin');
INSERT INTO `myl_shares` VALUES (6, '2020-08-21 14:08:45', '2020-08-21 14:09:24', NULL, '', 0, 1, 2, 4, 1, -1, NULL, 0, 1, 'key.bin');
INSERT INTO `myl_shares` VALUES (7, '2021-08-05 17:33:48', '2021-08-05 17:33:48', NULL, '', 0, 1, 9, 12, 0, -1, NULL, 100, 1, 'oss.png');
INSERT INTO `myl_shares` VALUES (8, '2021-08-05 20:02:13', '2021-09-01 16:52:46', NULL, '', 0, 1, 9, 25, 1, -1, NULL, 100, 1, 'oss.png');
INSERT INTO `myl_shares` VALUES (9, '2021-11-22 10:14:33', '2021-11-22 10:14:33', '2022-05-05 20:16:57', '', 0, 1, 5, 1, 0, -1, NULL, 0, 1, 'video.mp4');
INSERT INTO `myl_shares` VALUES (10, '2021-11-22 13:05:35', '2021-11-22 13:05:35', '2022-05-05 20:16:57', '', 0, 1, 5, 7, 0, -1, NULL, 0, 1, 'video.mp4');
INSERT INTO `myl_shares` VALUES (11, '2021-11-29 15:47:29', '2021-11-29 15:47:52', '2021-11-29 15:48:26', '', 0, 8, 25, 2, 1, -1, NULL, 0, 1, 'v.txt');
INSERT INTO `myl_shares` VALUES (12, '2021-11-29 18:23:03', '2021-11-29 18:23:25', NULL, '', 0, 8, 34, 1, 1, -1, NULL, 0, 1, 'v.txt');
INSERT INTO `myl_shares` VALUES (13, '2021-11-29 18:26:28', '2021-11-29 18:26:37', '2021-11-29 18:28:11', '', 0, 8, 36, 1, 1, -1, NULL, 0, 0, 'Desktop.zip');
INSERT INTO `myl_shares` VALUES (14, '2022-03-03 13:45:13', '2022-03-03 13:51:55', NULL, '', 1, 1, 8, 8, 2, -1, NULL, 0, 1, '333');
INSERT INTO `myl_shares` VALUES (15, '2022-06-17 10:41:26', '2022-06-17 14:36:03', NULL, '', 0, 1, 16, 10, 4, -1, NULL, 0, 1, '3333.mp3');
INSERT INTO `myl_shares` VALUES (16, '2022-06-30 09:09:03', '2022-06-30 09:09:50', '2022-06-30 09:10:35', '', 0, 1, 22, 2, 1, -1, NULL, 0, 1, '示例.xls');
INSERT INTO `myl_shares` VALUES (17, '2022-09-09 15:33:08', '2022-09-09 15:35:09', NULL, '', 1, 1, 8, 3, 1, -1, NULL, 0, 1, '333');

-- ----------------------------
-- Table structure for myl_storage_packs
-- ----------------------------
DROP TABLE IF EXISTS `myl_storage_packs`;
CREATE TABLE `myl_storage_packs`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `active_time` timestamp NULL DEFAULT NULL,
  `expired_time` timestamp NULL DEFAULT NULL,
  `size` bigint(20) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_myl_storage_packs_deleted_at`(`deleted_at`) USING BTREE,
  INDEX `expired`(`expired_time`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_storage_packs
-- ----------------------------
INSERT INTO `myl_storage_packs` VALUES (1, '2020-08-18 17:20:28', '2020-08-18 17:20:28', '2021-09-01 14:00:00', '10GB扩容包(联系客服免费扩容)', 1, '2020-08-18 17:20:28', '2021-08-18 17:20:28', 10737418240);
INSERT INTO `myl_storage_packs` VALUES (2, '2020-08-18 18:00:54', '2020-08-18 18:00:54', '2021-09-01 14:00:00', '100GB扩容包(联系客服免费扩容)', 5, '2020-08-18 18:00:54', '2021-08-18 18:00:54', 107374182400);
INSERT INTO `myl_storage_packs` VALUES (3, '2020-08-18 18:06:46', '2020-08-18 18:06:46', '2021-09-01 14:00:00', '100GB扩容包(联系客服免费扩容)', 5, '2020-08-18 18:06:46', '2021-08-18 18:06:46', 107374182400);
INSERT INTO `myl_storage_packs` VALUES (4, '2020-08-18 18:11:48', '2020-08-18 18:11:48', '2021-09-01 14:00:00', '100GB扩容包(联系客服免费扩容)', 5, '2020-08-18 18:11:48', '2021-08-18 18:11:48', 107374182400);
INSERT INTO `myl_storage_packs` VALUES (5, '2020-08-18 18:14:52', '2020-08-18 18:14:52', '2021-09-01 14:00:00', '100GB扩容包(联系客服免费扩容)', 3, '2020-08-18 18:14:52', '2021-08-18 18:14:52', 107374182400);
INSERT INTO `myl_storage_packs` VALUES (6, '2022-05-05 18:57:21', '2022-05-05 18:57:21', NULL, '100GB扩容包(联系客服免费扩容)', 1, '2022-05-05 18:57:21', '2023-05-05 18:57:21', 107374182400);
INSERT INTO `myl_storage_packs` VALUES (7, '2022-05-05 18:57:46', '2022-05-05 18:57:46', NULL, '100GB扩容包(联系客服免费扩容)', 9, '2022-05-05 18:57:46', '2023-05-05 18:57:46', 107374182400);

-- ----------------------------
-- Table structure for myl_tags
-- ----------------------------
DROP TABLE IF EXISTS `myl_tags`;
CREATE TABLE `myl_tags`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `expression` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_myl_tags_deleted_at`(`deleted_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_tags
-- ----------------------------

-- ----------------------------
-- Table structure for myl_tasks
-- ----------------------------
DROP TABLE IF EXISTS `myl_tasks`;
CREATE TABLE `myl_tasks`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `type` int(11) NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `progress` int(11) NULL DEFAULT NULL,
  `error` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `props` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `idx_myl_tasks_deleted_at`(`deleted_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_tasks
-- ----------------------------
INSERT INTO `myl_tasks` VALUES (1, '2020-08-11 10:22:55', '2020-08-11 10:22:57', NULL, 4, 2, 1, 0, '', '{\"src\":[\"/data/www/pan/tmp/aria2/1597112574885956492/default-avatar.jpg\"],\"parent\":\"/data/www/pan/tmp/aria2/1597112574885956492\",\"dst\":\"/\"}');

-- ----------------------------
-- Table structure for myl_users
-- ----------------------------
DROP TABLE IF EXISTS `myl_users`;
CREATE TABLE `myl_users`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `email` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `nick` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int(11) NULL DEFAULT NULL,
  `group_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `storage` bigint(20) UNSIGNED NULL DEFAULT NULL,
  `open_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `two_factor` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `options` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `authn` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `score` int(11) NULL DEFAULT NULL,
  `previous_group_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `group_expires` timestamp NULL DEFAULT NULL,
  `notify_date` timestamp NULL DEFAULT NULL,
  `alipay_card` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付宝账号',
  `alipay_name` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '支付宝真实姓名',
  `aid` int(11) NULL DEFAULT 0 COMMENT '邀请人',
  `last_ip` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '最后登录ip',
  `phone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uix_myl_users_email`(`email`) USING BTREE,
  INDEX `idx_myl_users_deleted_at`(`deleted_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_users
-- ----------------------------
INSERT INTO `myl_users` VALUES (1, '2020-08-10 19:58:45', '2023-01-03 09:11:03', NULL, 'admin@bilnn.com', '比邻云', 'N71Kn4zaPV6dvUBp:628b535508a94a157bb9edadc4b93fa4582eb11c', 0, 1, 153931014, '', '', 'gravatar', '{\"preferred_policy\":2}', '', 605, 0, NULL, NULL, '18866214266', '马运岭', 0, '127.0.0.1', '');
INSERT INTO `myl_users` VALUES (2, '2020-08-12 11:16:05', '2022-05-05 20:01:33', NULL, 'test@bilnn.com', 'test', '7hwbC1XSvow85WNt:6623473e9ce5d05794f886c9252d03b2cfb42893', 0, 2, 110411806, '', '', '', '{\"preferred_policy\":2}', '', 0, 0, NULL, NULL, NULL, NULL, 1, '127.0.0.1', NULL);
INSERT INTO `myl_users` VALUES (3, '2020-08-18 12:26:20', '2022-05-05 20:01:33', NULL, 'test1@bilnn.com', 'test1', 'txmpDnHOUcAOyEHV:7180cac1b4196e11dba8219502f27dfa1e343dea', 0, 2, 110411806, '', '', '', '{}', '', 800, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `myl_users` VALUES (4, '2020-08-18 12:27:37', '2022-07-15 15:52:58', NULL, 'test2@bilnn.com', 'test2', 'nADgS7UQekrpSZbM:cfecb48f64e155f1270c5bd5bb66ebe2d3cf09dc', 0, 2, 110422912, '', '', '', '{}', '', 0, 0, NULL, NULL, NULL, NULL, 1, '127.0.0.1', NULL);
INSERT INTO `myl_users` VALUES (5, '2020-08-18 13:46:10', '2022-05-05 20:01:33', NULL, 'test3@bilnn.com', 'test3', 'Usj0CB4diOPxhDpv:d6f85bae103a1bfe7a266118d9bfe783bcf47229', 0, 2, 110411806, '', '', '', '{}', '', 600, 0, NULL, NULL, NULL, NULL, 1, NULL, NULL);
INSERT INTO `myl_users` VALUES (6, '2021-08-05 19:50:34', '2022-05-05 20:01:33', NULL, 'ooh8NNTlGpx3ZL4s@login.qq.com', '你好世界', 'Vcuv8LlG5VIkd76c:c5a058c3cab57dc58a4ec55a9653627b6f287846', 0, 2, 110411806, '84B1A0C9F7EE3AF80ACF5E57279ABB8A', '', 'file', '{}', '', 0, 0, NULL, NULL, NULL, NULL, 0, NULL, NULL);
INSERT INTO `myl_users` VALUES (7, '2021-11-29 13:59:36', '2022-06-30 09:09:57', NULL, 'test10@bilnn.com', 'test10', 's3v91qC8HhteWM1z:153813dbe488228ba15576ca76bf7db55fdfa971', 0, 2, 110411806, '', '', '', '{}', '', 0, 0, NULL, NULL, NULL, NULL, 0, '127.0.0.1', NULL);
INSERT INTO `myl_users` VALUES (8, '2021-11-29 15:07:37', '2022-05-05 20:01:33', NULL, 'test11@bilnn.com', 'test11', 'fJvjjPmEFpg1lqYv:6715c17060df8ac0bfb3d81094ee8746e9fbcde9', 0, 2, 110411806, '', '', '', '{\"preferred_policy\":1}', '', 0, 0, NULL, NULL, NULL, NULL, 0, '127.0.0.1', NULL);
INSERT INTO `myl_users` VALUES (9, '2022-05-05 18:56:16', '2022-05-05 20:01:33', NULL, 'test111@bilnn.com', 'test111', 'DsEz8iOMBv48nsys:e076ef971277e60ea8f353add8452edc97d3d905', 0, 2, 110411806, '', '', '', '{}', '', 0, 0, NULL, NULL, NULL, NULL, 1, '127.0.0.1', '');

-- ----------------------------
-- Table structure for myl_webdavs
-- ----------------------------
DROP TABLE IF EXISTS `myl_webdavs`;
CREATE TABLE `myl_webdavs`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NULL DEFAULT NULL,
  `root` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `password_only_on`(`password`, `user_id`) USING BTREE,
  INDEX `idx_myl_webdavs_deleted_at`(`deleted_at`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of myl_webdavs
-- ----------------------------
INSERT INTO `myl_webdavs` VALUES (1, '2020-08-11 10:57:23', '2020-08-11 10:57:23', NULL, 'myxf', 'm64Xn2nGpl30QJ63pMVSwRY4TlMeTGfK', 1, '/');
INSERT INTO `myl_webdavs` VALUES (2, '2020-08-11 10:57:00', '2020-08-11 10:57:00', NULL, 'test', 'zVquy90qehZWAZBWtzjZcbjucXiQx6N8', 4, '/');

SET FOREIGN_KEY_CHECKS = 1;
