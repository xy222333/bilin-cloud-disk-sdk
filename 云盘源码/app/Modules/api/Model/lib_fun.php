<?php
class lib_fun extends mModel
{
    public function parError($code = 1,$msg = '参数错误',$backdata = array()){
        if (is_array($code)) exit(json_encode($code,JSON_UNESCAPED_UNICODE));
        exit(json_encode(['code'=>$code,'msg'=>$msg,'backdata'=>$backdata],JSON_UNESCAPED_UNICODE));
    }

    //获取用户id
    public function getUserId($token){
        $ext_fun = $this->mClass("ext_fun");
        $user_id = 0;
        $token = $ext_fun->tostring($token);
        if (!empty($token['user_id'])) $user_id =  intval($token['user_id']);
        if (empty($user_id)) $this->parError(-1,'鉴权错误');
        return $user_id;
    }
}