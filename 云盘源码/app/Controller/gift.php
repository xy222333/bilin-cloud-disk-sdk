<?php
if(!defined('APP_VER')) exit("die!");
class gift extends App
{	
	function __construct(){
		parent::__construct();
		$lib_api = $this->mModel("lib_api");
		$lib_api->ckLogin();
	}

	//商城首页
	public function index(){
		$lib_gift = $this->mModel("lib_gift");
		$lib_user = $this->mModel("lib_user");
		$user_id = $this->user_id;
		$user = $lib_user->getUserInfo($user_id);
	    $gift = $lib_gift->getGiftList();
	    $backurl = empty($this->mArgs("backurl")) ? "" : urlencode($this->mArgs("backurl"));

	    $this->gift = $gift;
	    $this->user = $user;
	    $this->action = "index";
	    $this->title = "在线商城";
	    $this->backurl = $backurl;
	    $this->display("gift/gift_index.html");
	}

	//购买礼物
	public function buy(){
		$lib_gift = $this->mModel("lib_gift");

		$user_id = $this->user_id;
		$gift_id = intval($_POST['gift_id']);
		$num = empty(intval($_POST['num'])) ? 1 : intval($_POST['num']);
		if (empty($user_id)){
			$return['code'] = 1;
			$return['msg'] = "请先登录";
		}else{
			$return = $lib_gift->bugGift($gift_id,$num,$user_id);
		}
		exit(json_encode($return));
	}

	//我的背包
	public function my_gift(){
		$lib_user = $this->mModel("lib_user");
		$lib_gift = $this->mModel("lib_gift");
		$user_id = $this->user_id;
		$user = $lib_user->getUserInfo($user_id);
		$gift = $lib_gift->getMyGift($user_id);
		$backurl = empty($this->mArgs("backurl")) ? "" : urlencode($this->mArgs("backurl"));

	    $this->gift = $gift;
	    $this->user = $user;
	    $this->action = "my_gift";
	    $this->title = "我的背包";
	    $this->backurl = $backurl;
	    $this->display("gift/gift_index.html");
	}

	//礼物记录
	public function my_gift_log(){
		$lib_user = $this->mModel("lib_user");
		$M = $this->mModel("lib_db");

		$user_id = $this->user_id;
		$user = $lib_user->getUserInfo($user_id);
		$limit = 10;
		$page = empty($_GET['page']) ? 1 : intval($_GET['page']);

		$M->db("my_gift_log");
		$pager = $M->pager("*","user_id=".$user_id,$page,$limit);
		$gift = $pager['list'];
		foreach ($gift as $key => $one) {
			$gift[$key]['ctime'] = date('m-d H:i:s',$one['ctime']);
			$M->db("gift");
			$gift[$key]['gift_name'] = $M->getData("gift_name","id=".$one['gift_id']);
			$gift[$key]['gift_pic'] = $M->getData("gift_pic","id=".$one['gift_id']);
		}
		$backurl = empty($this->mArgs("backurl")) ? "" : urlencode($this->mArgs("backurl"));

		$this->gift = $gift;
		$this->pager = $pager;
		$this->user = $user;
	    $this->action = "my_gift_log";
	    $this->title = "礼物记录";
	    $this->backurl = $backurl;
	    $this->display("gift/gift_index.html");
	}

	//赠送礼物
	public function send_gift(){
		$M = $this->mModel("lib_db");
		$lib_user = $this->mModel("lib_user");
		$lib_gift = $this->mModel("lib_gift");
		$lib_api = $this->mModel("lib_api");

		$user_id = $this->user_id;
		$id = intval($lib_api->hashids_decode($_GET['id'])[0]);
		$M->db("shares");
		$shares = $M->getOne("id,source_name,user_id","id=".$id);
		if (!$shares) exit("分享文件未找到");
		$f_user = $lib_user->getUserInfo($shares['user_id']);
		$gift = $lib_gift->getMyGift($user_id);

	    $this->gift = $gift;
	    $this->shares = $shares;
	    $this->f_user = $f_user;
		$this->title = "打赏礼物";
		$this->action = "send_gift";
		$this->id = $_GET['id'];
	    $this->display("gift/gift_send.html");
	}

	//处理打赏
	public function do_send_gift(){
		$lib_user = $this->mModel("lib_user");
		$lib_gift = $this->mModel("lib_gift");
		$lib_api = $this->mModel("lib_api");

		$user_id = $this->user_id;
		$gift_id = intval($_POST['gift_id']);
		$num = empty(intval($_POST['num'])) ? 1 : intval($_POST['num']);
		$sid = intval($_POST['sid']);
		$return = $lib_gift->sendGift($gift_id,$num,$sid,$user_id);
		exit(json_encode($return));
	}

	//打赏列表
	public function send_gift_log(){
		$M = $this->mModel("lib_db");
		$lib_api = $this->mModel("lib_api");

		$user_id = $this->user_id;
		$id = intval($lib_api->hashids_decode($_GET['id'])[0]);
		$limit = 10;
		$page = empty($_GET['page']) ? 1 : intval($_GET['page']);
		$M->db("shares");
		$shares = $M->getOne("id,source_name,user_id","id=".$id);
		if (!$shares) exit("分享文件未找到");

		$M->db("my_gift_log");
		$pager = $M->pager("*","fid=".$shares['id'],$page,$limit);
		$gift = $pager['list'];
		foreach ($gift as $key => $one) {
			$gift[$key]['ctime'] = date('m-d H:i:s',$one['ctime']);
			$M->db("gift");
			$gift[$key]['gift_name'] = $M->getData("gift_name","id=".$one['gift_id']);
			$gift[$key]['gift_pic'] = $M->getData("gift_pic","id=".$one['gift_id']);
			$gift[$key]['gift_num'] = abs($one['gift_num']);
			$M->db("users");
			$user = $M->getOne("id,nick","id=".$one['user_id']);
			$gift[$key]['username'] = $user['nick'];
		}

	    $this->gift = $gift;
	    $this->pager = $pager;
	    $this->shares = $shares;
		$this->title = "打赏列表";
		$this->action = "send_gift_log";
		$this->id = $_GET['id'];
	    $this->display("gift/gift_send.html");
	}
}
?>