<?php
if(!defined('APP_VER')) exit("die!");
class main extends App
{	
	function __construct(){
		parent::__construct();
		$url = $_SERVER['PHP_SELF'];
		if (strpos($url, 'index.php/')!==false) header("location:/index.php?".http_build_query($_GET));
	}

	public function index(){
	    $REFERER = $this->mArgs("referer");
	    $host = "";
	    if (!empty($REFERER)){
	        $parts = parse_url($REFERER);
	        if($parts['host']=="pan.bilnn.com" || $parts['host']=="pan.bilnn.cn"){
	            setcookie("REFERER", $parts['host']);
	            $host = $parts['host'];
	        }
	    }
	    $this->host = $host;
		$this->display("index.html");
	}

	public function apps(){
		$this->display("apps.html");
	}

	public function news(){
		$id = intval($this->mArgs("id"));
		if(empty($id)) $id = 1;
		if ($id==1){
			$this->display("news.html");
		}
		if ($id==2){
			$this->display("news_custom.html");
		}
	}

	public function update(){
		$appid = $this->mArgs("appid");
		$version = $this->mArgs("version");
		$rsp = array('status' => 0);
		if (isset($appid) && isset($version)) {  
		    if($appid=="__W2A__pan.bilnn.com" || $appid=="__W2A__pan.bilnn.cn" || $appid=="HBuilder"){
		        if($version !== "1.2"){  
		            $rsp['status'] = 1;  
		            $rsp['title'] = "应用更新";  
		            $rsp['note'] = "修复bug；\n优化用户体验;";
		            $rsp['url'] = "https://pan.bilnn.cn/app/index.php?a=getDownUrl&device=android";
		        }  
		    }  
		}   
		exit(json_encode($rsp));
	}

	public function download(){
		$this->display("download.html");
	}

	public function getDownUrl(){
		$device = $this->mArgs("device","windows");
		if ($device=="windows") {
		    if ($this->mArgs("action")=="update"){
		        header("location:https://store.yundingmeta.com/music/music/BilPan Setup.exe");
		        exit();
		    }
		    header("location:https://store.yundingmeta.com/music/music/BilPan Setup.zip");
		}
		if ($device=="android") header("location:https://store.yundingmeta.com/music/music/BilnnPan.apk");
	}
	
	public function delFile(){
	    set_time_limit(0);
	    $M = $this->mModel("lib_db");

	    $M->db("policies");
	    $policies = $M->getAll("options,bucket_name,access_key,secret_key","bucket_name<>'' group by bucket_name");

	    $M->db("users");
	    $mytime = mktime(0, 0, 0, date('m')-6, date('d'), date('Y'));
        $mytime = date("Y-m-d H:i:s", strtotime("-6 month"));
        
        //先将只转存的用户清理掉
	    $users = $M->getAll("id","`updated_at` <= '".$mytime."' AND `storage` > '99988050'","`storage` DESC");
	    foreach ($users as $key => $one) {
	        $M->db("shares");
        	$shares = $M->getCount("user_id=".$one['id']);
        	if (intval($shares)>0) continue; //如果有文件分享就跳过
            $M->db("files");
            $files_start = $M->getAll("user_id,source_name","user_id=".$one['id'],"id asc",2);
            $files_end = $M->getAll("user_id,source_name","user_id=".$one['id'],"id desc",1);
            if(strpos($files_start[1]['source_name'],"uploads/".$one['id']."/")===false && strpos($files_end[0]['source_name'],"uploads/".$one['id']."/")===false){
                $M->del("user_id=".$one['id']);
                $M->db("folders");
                $M->del("owner_id=".$one['id']);
                $M->db("users");
                $M->edit(['storage'=>0],"id=".$one['id']);
            }
        }

        //继续清理剩余用户
        $M->db("users");
        $users = $M->getAll("id","`updated_at` <= '".$mytime."' AND `storage` > '99988050'","`storage` DESC");
        foreach ($users as $key => $one) {
            $M->db("shares");
        	$shares = $M->getCount("user_id=".$one['id']);
        	if (intval($shares)>0) continue; //如果有文件分享就跳过
        	$M->db("files");
        	$M->del("user_id=".$one['id']);
        	$M->db("folders");
            $M->del("`name`!='/' and owner_id=".$one['id']);
        	$M->db("users");
            $M->edit(['storage'=>0],"id=".$one['id']);
            //删除远程文件
            foreach ($policies as $keys => $ones) {
    	    	$ext_onedrive = $this->mClass("ext_onedrive",array($ones['options'],$ones['bucket_name'],$ones['secret_key'],$ones['access_key']));
    		    if (!empty($one['id'])) $ext_onedrive->del_file("uploads/".$one['id']."/");
    	    }
        }
        echo "ok";
	}
	
	public function delFileOne(){
	    $M = $this->mModel("lib_db");
	    //继续清理剩余用户
	    $one['key'] = $this->mArgs("key");
        $one['id'] = intval($this->mArgs("id"));
        if ($one['key']!="wef56456456we") exit("err");
        
        $M->db("policies");
	    $policies = $M->getAll("options,bucket_name,access_key,secret_key","bucket_name<>'' group by bucket_name");
        
    	$M->db("files");
    	$M->del("user_id=".$one['id']);
    	$M->db("folders");
        $M->del("`name`!='/' and owner_id=".$one['id']);
    	$M->db("users");
        $M->edit(['storage'=>0],"id=".$one['id']);
        $M->db("shares");
        $M->del("user_id=".$one['id']);
        //删除远程文件
        foreach ($policies as $keys => $ones) {
	    	$ext_onedrive = $this->mClass("ext_onedrive",array($ones['options'],$ones['bucket_name'],$ones['secret_key'],$ones['access_key']));
		    if (!empty($one['id'])) $ext_onedrive->del_file("uploads/".$one['id']."/");
	    }
       
        echo "ok";
	}
	
	public function gifts(){
	    exit();
	    $M = $this->mModel("lib_db");
	    $M->db("users");
	    $mytime = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
	    $users = $M->getAll("id,group_expires","group_id=4 and `group_expires` >= '".$mytime."' ");
	    foreach ($users as $key => $one) {
	        $update = array();
	        $update['group_expires'] = date("Y-m-d H:i:s", strtotime("+1 month",strtotime($one['group_expires'])));
	        echo "update myl_users set group_expires='".$update['group_expires']."' where id=".$one['id'].";\r\n";
	    }
	}
	
	public function baidupush(){
		$lib_api = $this->mModel("lib_api");
		$M = $this->mModel("lib_db");
		$M->db("shares");
		$shares = $M->getAll("id","","id desc",2000);
		$urls = [];
		foreach ($shares as $key => $one) {
			$hashids = $lib_api->hashids_encode(intval($one['id']),0);
			$urls[] = 'https://pan.bilnn.com/s/'.$hashids;
		}
		$api = 'http://data.zz.baidu.com/urls?site=https://pan.bilnn.com&token=FSvnIeMhJYBEMjOZ';
		$ch = curl_init();
		$options =  array(
		    CURLOPT_URL => $api,
		    CURLOPT_POST => true,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POSTFIELDS => implode("\n", $urls),
		    CURLOPT_HTTPHEADER => array('Content-Type: text/plain'),
		);
		curl_setopt_array($ch, $options);
		$result = curl_exec($ch);
		echo $result;
	}
}
?>