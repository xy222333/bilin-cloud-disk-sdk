<?php
class lib_db extends mModel
{
  var $pk = "id"; // 数据表的主键
  var $table = ""; // 数据表的名称
  var $prefix = "myl_"; //前缀

    //事务锁表
    public function mysql_start(){   
        mysqli_autocommit($this->mysqli->conn,FALSE);
    }

    //提交事务
    public function mysql_commit(){   
        mysqli_commit($this->mysqli->conn);
        mysqli_autocommit($this->mysqli->conn,TRUE);
    }

    /**
     * 设置表名称
     * @param var $dname 表名
     */
    public function db($dbname){
       $this->table = $this->prefix.$dbname;
    }
    //关闭连接
    public function close(){
        $this->mysqliclose();
    }
    /**
     * 获取全部数据
     * @param var $field 字段
     * @param var $where 条件
     * @param var $sort 排序
     * @param var $limit 读取记录数量
     */
    public function getAll($field = "*",$where = "",$sort = "",$limit = ""){
        $where = empty($where) ? "1=1" : $where;
        $sort = empty($sort) ? "" : "order by ".$sort;
        $limit = empty($limit) ? $limit : "limit ".$limit;
        $sql="Select ".$field." from `".$this->table."` where ".$where." ".$sort." ".$limit;
        $result = $this->sql($sql);
        return $result;
    }

    /**
     * 执行Sql语句
     * @param var $sql sql语句
     */
    public function sql($sql){
        $result = $this->findsql($sql);
        return $result;
    }

    /**
     * 获取一条数据
     * @param var $field 字段
     * @param var $where 条件
     */
    public function getOne($field = "*",$where = "1=1"){
        $sql="Select ".$field." from `".$this->table."` where ".$where." limit 1";
        $result = $this->sql($sql);
        return empty($result[0]) ? null : $result[0];
    }

    /**
     * 获取数据总数
     * @param var $where
     */
    public function getCount($where = "1=1"){
        $sql="Select count(*) as num from `".$this->table."` where ".$where;
        $result = $this->sql($sql);
        return $result[0]["num"];
    }

    /**
     * 获取数据总数去重
     * @param var $table
     * @param var $where
     */
    public function getCounts($table,$where = "1=1"){
        $sql="Select count(distinct ".$table.") as num from `".$this->table."` where ".$where;
        $result = $this->sql($sql);
        return $result[0]["num"];
    }

    /**
     * 增加一条数据并返回自增id
     * @param array $data
     */
    public function add($data){
    	$column = array();
		$datas = array();
		foreach ($data as $key => $one) {
			$column[] = "`".$key."`";
			$datas[] = "'".addslashes($one)."'";
		}
		$column = implode(",", $column);
		$datas = implode(",", $datas);
		$sql="insert into `".$this->table."` (".$column.")values(".$datas.")";
        $result = $this->create($sql,$this->table);  // 进行新增操作    
        return $result;
    }

    /**
     * 编辑一条数据并返回修改成功数
     * @param array $data
     * @param var $where
     */
    public function edit($data,$where = "1=1"){
        $column = array();
        foreach ($data as $key => $one) {
            $column[] = "`".$key."` = '".$one."'";
        }
        $column = implode(",", $column);
        $sql="update `".$this->table."` set ".$column." where ".$where;
        $result = $this->edits($sql);
        return $result;
    }

    /**
     * 删除数据若where为空则删除全部
     * @param var $where
     */
    public function del($where = "1=1"){
        $sql="delete from `".$this->table."` where ".$where;
        $result = $this->edits($sql);
        return $result;
    }

    /**
     * 获取一条字段数据
     * @param var $field 字段
     * @param var $where 条件
     * @param var $empty 默认值
     */
    public function getData($field,$where = "1=1",$empty = ""){
        $sql="Select ".$field." from `".$this->table."` where ".$where." limit 1";
        $result = $this->sql($sql);
        if (!empty($empty) && empty($result[0][$field])){
           return $empty;
        }
        return $result[0][$field];
    }

    /**
     * 获取求和数据
     * @param var $where table
     * @param var $where 条件
     */
    public function getSum($table,$where="1=1"){
        $sql="Select sum(".$table.") as num from `".$this->table."` where ".$where;
        $result = $this->sql($sql);
        $result = empty($result[0]["num"]) ? 0 : $result[0]["num"];
        return $result;
    }

    /**
     * 分页获取数据
     * @param var $field 字段
     * @param var $where 条件
     * @param var $pageNo 页码
     * @param var $pageSize 每页条数
     * @param var $sort 排序
     */
    public function pager($field = "*",$where = "1=1",$pageNo = 1,$pageSize = 10,$order = "id desc"){
        $where = empty($where) ? "1=1" : $where;
        $limit = ($pageNo - 1) * $pageSize;
        $sql = "Select ".$field." from ".$this->table." where ".$where." order by ".$order." limit ".$limit.",".$pageSize;
        $result = $this->sql($sql);
        $pager['total'] = count($this->sql("Select ".$field." from ".$this->table." where ".$where));
        $pager['limit'] = $pageSize;
        $pager['now'] = $pageNo;
        $pager['list'] = $result;
        return $pager;
    }

}