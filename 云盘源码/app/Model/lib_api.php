<?php
class lib_api extends mModel
{
  
	public function ckLogin(){
		if(intval($_SESSION['user_id'])===0) $this->mAlert("登录失效，请重新登录",null,null,false);
	}

	//api鉴权
	public function ckAuth($secret_key){
		if (empty($secret_key)) exit(json_encode(['code'=>401,'msg'=>'权限验证失败']));
		$secret_key = explode(":", base64_decode($secret_key));
		$email = $secret_key[0];
		$secret_key = addslashes($secret_key[1]);
		if (empty($secret_key)) exit(json_encode(['code'=>401,'msg'=>'权限验证失败']));
		$M = $this->mModel("lib_db");
		$M->db("webdavs");
		$webdavs = $M->getOne("user_id","password='".$secret_key."'");
		if (!$webdavs) exit(json_encode(['code'=>401,'msg'=>'权限验证失败']));
		$M->db("users");
		$user = $M->getOne("id","status=0 and id=".$webdavs['user_id']);
		if (!$user) exit(json_encode(['code'=>401,'msg'=>'账号已被禁用']));
		$_SESSION['user_id'] = $webdavs['user_id'];
	}

	public function hashids_encode($string,$types = 2){
		require_once(__DIR__.'/../Ext/Hashids.php');
		$hashids = new Hashids\Hashids('DXrpwJkS6ARInwR7L1xqLgUNwtyGUPMebbffWUptzmOO43JYiePjZLhRqdSBZeWc');
		return $hashids->encode($string,$types);
	}

	public function hashids_decode($string,$types = 2){
		require_once(__DIR__.'/../Ext/Hashids.php');
		$hashids = new Hashids\Hashids('DXrpwJkS6ARInwR7L1xqLgUNwtyGUPMebbffWUptzmOO43JYiePjZLhRqdSBZeWc');
		return $hashids->decode($string,$types);
	}
}