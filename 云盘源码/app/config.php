<?php
header('Content-type: text/html; charset=UTF-8');
date_default_timezone_set('PRC');
define("APP_PATH",dirname(__FILE__));
define("APP_VER","1.2");
define("Modules", basename($_SERVER["SCRIPT_NAME"],".php")=='index' ? "" : basename($_SERVER["SCRIPT_NAME"],".php")); // 模块名称
$modules = empty(Modules) ? "" : '/Modules/'.Modules;
define("Controller_Path", APP_PATH.$modules.'/Controller/'); //控制器目录
define("Tmp_Path", APP_PATH.$modules.'/View/'); // 模板存放的目录
define("Ext_Path", APP_PATH.'/Ext/'); //扩展目录目录
define("pan_path", "https://".$_SERVER['SERVER_NAME']); //网盘主站地址
require_once APP_PATH.'/vendor/autoload.php';
//配置文件类
class minConfig
{
    public $minConfig = array(
        "Config" => array( 
                'Name' => 'phpMin框架',
                'Author' => 'Myxf', 
                'gitee' => 'https://gitee.com/mayoushang/phpmin',
                'debug' => true
        ),
        "Db" => array( // 数据库设置
                'driver' => 'mysqli',   // 驱动类型
                'host' => '127.0.0.1',  // 数据库地址，一般都可以是localhost
                'login' => 'root', // 数据库用户名
                'password' => 'root', // 数据库密码
                'database' => 'pan', // 数据库的库名称
                'port' => 3306 //端口
        ),
        "View" => array(
                'view_path' => Tmp_Path, // 模板存放的目录
                'tmp_path' => APP_PATH.'/Tmp/', // 编译的临时目录
                'left_delimiter' => '{',  // smarty左限定符
                'right_delimiter' => '}', // smarty右限定符
                'caching' => False, // 开启缓存
                'cache_lifetime' => 3600, // 页面缓存1小时
        )
    );
}
?>