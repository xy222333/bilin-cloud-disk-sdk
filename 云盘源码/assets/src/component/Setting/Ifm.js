import React, { Component } from "react";
import { useParams } from "react-router";

export default function Ifm() {
    const params = useParams("action");
	let iFrameHeight = '500px';
	let iframe = null;

    const getCookie =(name)=> {
        const strcookie = document.cookie;//获取cookie字符串
            const arrcookie = strcookie.split("; ");//分割
            //遍历匹配
            for ( let i = 0; i < arrcookie.length; i++) {
                const arr = arrcookie[i].split("=");
                if (arr[0] == name){
                    return unescape(arr[1]);
                }
            }
        return null;
    }
	
    const ifmUrl = ()=>{
        return "/app/index.php?" + params.action + "&token=" + getCookie("token");
    }
     return (
        <iframe 
                style={{width:'100%', overflow:'visible'}}
                onLoad={() => {
					iFrameHeight = (document.documentElement.clientHeight - (document.documentElement.clientHeight * 0.075)) + 'px';
					iframe.height = iFrameHeight;
                }} 
				ref={(c) => { iframe = c; }}
                src={ifmUrl()} 
                width="100%" 
                height={iFrameHeight} 
                frameBorder="0"
				scrolling="auto"
            />
     );
}