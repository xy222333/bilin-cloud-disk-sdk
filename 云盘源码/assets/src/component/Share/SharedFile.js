import React, { Component } from "react";
import AdSense from 'react-adsense';
import { connect } from "react-redux";
import { sizeToString, vhCheck } from "../../utils";
import { isPreviewable } from "../../config";
import { Button, Typography, withStyles } from "@material-ui/core";
import Divider from "@material-ui/core/Divider";
import TypeIcon from "../FileManager/TypeIcon";
import Auth from "../../middleware/Auth";
import PurchaseShareDialog from "../Modals/PurchaseShare";
import { withRouter } from "react-router-dom";
import Creator from "./Creator";
import pathHelper from "../../utils/page";
import Report from "../Modals/Report";
import IconButton from "@material-ui/core/IconButton";
import MonetizationOnIcon from '@material-ui/icons/MonetizationOn';
import {
    openMusicDialog,
    openResaveDialog,
    setSelectedTarget,
    showImgPreivew,
    toggleSnackbar,
} from "../../redux/explorer";
import { startDownload } from "../../redux/explorer/action";
import { trySharePurchase } from "../../redux/explorer/async";

vhCheck();
const styles = (theme) => ({
    layout: {
        width: "auto",
        marginTop: "90px",
        marginLeft: theme.spacing(3),
        marginRight: theme.spacing(3),
        [theme.breakpoints.up(1100 + theme.spacing(3) * 2)]: {
            width: 1100,
            marginTop: "90px",
            marginLeft: "auto",
            marginRight: "auto",
        },
        [theme.breakpoints.down("sm")]: {
            marginTop: 0,
            marginLeft: 0,
            marginRight: 0,
        },
        justifyContent: "center",
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    },
    player: {
        borderRadius: "4px",
    },
    fileCotainer: {
        width: "200px",
        margin: "0 auto",
    },
    buttonCotainer: {
        width: "400px",
        margin: "0 auto",
        textAlign: "center",
        marginTop: "20px",
    },
    paper: {
        padding: theme.spacing(2),
    },
    icon: {
        borderRadius: "10%",
        marginTop: 2,
    },

    box: {
        width: "100%",
        maxWidth: 440,
        backgroundColor: theme.palette.background.paper,
        borderRadius: 12,
        boxShadow: "0 8px 16px rgba(29,39,55,.25)",
        [theme.breakpoints.down("sm")]: {
            /*height: "calc(var(--vh, 100vh) - 56px)",*/
            borderRadius: 0,
            maxWidth: 1000,
        },
        display: "flex",
        flexDirection: "column",
    },
    boxContent: {
        padding: 24,
        display: "flex",
        flex: "1",
    },
    fileName: {
        marginLeft: 20,
    },
    fileSize: {
        color: theme.palette.text.disabled,
        fontSize: 14,
    },
    boxFooter: {
        display: "flex",
        padding: "20px 16px",
        justifyContent: "space-between",
    },
    downloadButton: {
        marginLeft: 8,
    },
    ads:{
        width: '100%',
        height: 90,
        maxHeight: 90,
        textAlign: "center",
        marginTop: "1.5rem"
    }
});
const mapStateToProps = () => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {
        toggleSnackbar: (vertical, horizontal, msg, color) => {
            dispatch(toggleSnackbar(vertical, horizontal, msg, color));
        },
        openMusicDialog: () => {
            dispatch(openMusicDialog());
        },
        setSelectedTarget: (targets) => {
            dispatch(setSelectedTarget(targets));
        },
        showImgPreivew: (first) => {
            dispatch(showImgPreivew(first));
        },
        openResave: (key) => {
            dispatch(openResaveDialog(key));
        },
        startDownload: (share, file) => {
            dispatch(startDownload(share, file));
        },
        trySharePurchase: (share) => dispatch(trySharePurchase(share)),
    };
};

const Modals = React.lazy(() => import("../FileManager/Modals"));
const ImgPreview = React.lazy(() => import("../FileManager/ImgPreview"));

class SharedFileCompoment extends Component {
    state = {
        anchorEl: null,
        open: false,
        purchaseCallback: null,
        loading: false,
        openReport: false,
    };

    downloaded = false;

    // TODO merge into react thunk
    preview = () => {
        if (pathHelper.isSharePage(this.props.location.pathname)) {
            if (!Auth.Check()) {
                this.props.toggleSnackbar(
                    "top",
                    "right",
                    "请先登录",
                    "warning"
                );
                return;
            }
            const user = Auth.GetUser();
            if (!Auth.Check() && user && !user.group.shareDownload) {
                this.props.toggleSnackbar(
                    "top",
                    "right",
                    "请先登录",
                    "warning"
                );
                return;
            }
        }

        switch (isPreviewable(this.props.share.source.name)) {
            case "img":
                this.props.showImgPreivew({
                    key: this.props.share.key,
                    name: this.props.share.source.name,
                });
                return;
            case "msDoc":
                this.props.history.push(
                    this.props.share.key +
                        "/doc?name=" +
                        encodeURIComponent(this.props.share.source.name)
                );
                return;
            case "audio":
                this.props.setSelectedTarget([
                    {
                        key: this.props.share.key,
                        type: "share",
                    },
                ]);
                this.props.openMusicDialog();
                return;
            case "video":
                this.props.history.push(
                    this.props.share.key +
                        "/video?name=" +
                        encodeURIComponent(this.props.share.source.name)
                );
                return;
            case "edit":
                this.props.history.push(
                    this.props.share.key +
                        "/text?name=" +
                        encodeURIComponent(this.props.share.source.name)
                );
                return;
            case "pdf":
                this.props.history.push(
                    this.props.share.key +
                        "/pdf?name=" +
                        encodeURIComponent(this.props.share.source.name)
                );
                return;
            case "code":
                this.props.history.push(
                    this.props.share.key +
                        "/code?name=" +
                        encodeURIComponent(this.props.share.source.name)
                );
                return;
            case "epub":
                this.props.history.push(
                    this.props.share.key +
                        "/epub?name=" +
                        encodeURIComponent(this.props.share.source.name)
                );
                return;
            default:
                this.props.toggleSnackbar(
                    "top",
                    "right",
                    "此文件无法预览",
                    "warning"
                );
                return;
        }
    };

    scoreHandler = (callback) => (event) => {
        this.props.trySharePurchase(this.props.share).then(() => callback());
    };

    componentWillUnmount() {
        this.props.setSelectedTarget([]);
    }

    download = () => {
        this.props.startDownload(this.props.share, null);
    };

    rewards = () => {
        if (!Auth.Check()) {
            this.props.toggleSnackbar(
                "top",
                "right",
                "登录后才能继续操作",
                "warning"
            );
            return;
        }
        window.opensendgift(this.props.share.key);
    };

    render() {
        const { classes } = this.props;
        const user = Auth.GetUser();
        const isLogin = Auth.Check();

        return (
            <div className={classes.layout}>
                <Modals />
                <ImgPreview />
                <PurchaseShareDialog />
                <Report
                    open={this.state.openReport}
                    share={this.props.share}
                    onClose={() => this.setState({ openReport: false })}
                />
                <div className={classes.box}>
                    <Creator share={this.props.share} />
                    <Divider />
                    <div className={classes.boxContent}>
                        <TypeIcon
                            className={classes.icon}
                            isUpload
                            fileName={this.props.share.source.name}
                        />
                        <div className={classes.fileName}>
                            <Typography style={{ wordBreak: "break-all" }}>
                                {this.props.share.source.name}
                            </Typography>
                            <Typography className={classes.fileSize}>
                                {sizeToString(this.props.share.source.size)}(不能下载，下载设置中取消“下载加速”)
                            </Typography>
                        </div>
                    </div>
                    <Divider />
                    <div className={classes.boxFooter}>
                        <div className={classes.actionLeft}>
                            <Button
                                onClick={() =>
                                    this.props.openResave(this.props.share.key)
                                }
                                color="secondary"
                            >
                                保存文件
                            </Button>
                            <Button
                                onClick={() =>
                                    this.setState({ openReport: true })
                                }
                                color="secondary"
                            >
                                举报
                            </Button>
                        </div>
                        <div className={classes.actions}>
                            <IconButton color="secondary" style={{ padding:'unset',paddingRight:'12px'}} title="打赏" aria-label="to reward" onClick={() => this.rewards()}>
                              <MonetizationOnIcon />
                            </IconButton>
                            {this.props.share.preview && (
                                <Button
                                    variant="outlined"
                                    color="secondary"
                                    onClick={this.scoreHandler(this.preview)}
                                    disabled={this.state.loading}
                                >
                                    预览
                                </Button>
                            )}
                            <Button
                                variant="contained"
                                color="secondary"
                                className={classes.downloadButton}
                                onClick={this.download}
                                disabled={this.state.loading}
                            >
                                下载
                                {this.props.share.score > 0 &&
                                    (!isLogin || !user.group.shareFree) &&
                                    " (" + this.props.share.score + "积分)"}
                                {this.props.share.score > 0 &&
                                    isLogin &&
                                    user.group.shareFree &&
                                    " (免积分)"}
                            </Button>
                        </div>
                    </div>
                </div>
                <div className={classes.ads}>
                    <AdSense.Google
                        style={{ display:'inline-block',minWidth:320,maxWidth:970,width:'100%',height:90 }}
                        client='ca-pub-7329474538416487'
                        slot='8294203043'
                        format=''
                    />
                </div>
                <div className={classes.ads}>
                    <a href={"https://feige.bilnn.com/"} target="_blank" rel="noopener noreferrer"><img style={{ height:90 }} src={"https://dd-static.jd.com/ddimg/jfs/t1/116251/16/25590/22224/62afe45aE4c00d47a/96f0ecbf9a568488.png"} /></a>
                </div>
            </div>
        );
    }
}

const SharedFile = connect(
    mapStateToProps,
    mapDispatchToProps
)(withStyles(styles)(withRouter(SharedFileCompoment)));

export default SharedFile;
