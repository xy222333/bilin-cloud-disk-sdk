import React from "react";
import SentimentVeryDissatisfiedIcon from "@material-ui/icons/SentimentVeryDissatisfied";
import { lighten, makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
    icon: {
        fontSize: "160px",
    },
    emptyContainer: {
        bottom: "0",
        height: "450px",
        margin: "50px auto",
        color: lighten(theme.palette.text.disabled, 0.4),
        textAlign: "center",
        paddingTop: "20px",
    },
    emptyInfoBig: {
        fontSize: "25px",
        color: lighten(theme.palette.text.disabled, 0.4),
    },
    ads:{
        width: '100%',
        height: 90,
        maxHeight: 90,
        textAlign: "center",
        marginTop: "1.5rem"
    },
    img:{
        width: '100%',
        maxWidth: '500px',
    }
}));

export default function Notice(props) {
    const classes = useStyles();
    return (
        <div className={classes.emptyContainer}>
            <SentimentVeryDissatisfiedIcon className={classes.icon} />
            <div className={classes.emptyInfoBig}>{props.msg}</div>
            <div className={classes.ads}>
                <a href={"https://feige.bilnn.com/"} target="_blank" rel="noopener noreferrer"><img className={classes.img} src={"https://dd-static.jd.com/ddimg/jfs/t1/70436/9/17642/20526/62733560E6424623a/ce77c61c12032c41.png"} /></a>
            </div>
        </div>
    );
}
