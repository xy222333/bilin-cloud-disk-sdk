﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    internal class tool
    {

        /// <summary>
        /// 计算大小string.Format("进度：{0:0.0%} 下载速度：{1}/S", taskInfo.Percent, StrHelper.CountSize((long)taskInfo.Speed));
        /// </summary>
        /// <param name="Size">大小byte</param>
        /// <returns></returns>
        internal static string 计算大小(long Size)
        {
            string result = "";
            long num = Size;
            bool flag = (double)num < 1024.0;
            if (flag)
            {
                result = (num.ToString("F2") ?? "");
            }
            else
            {
                bool flag2 = (double)num >= 1024.0 && num < 1048576L;
                if (flag2)
                {
                    result = ((double)num / 1024.0).ToString("F2") + "KB";
                }
                else
                {
                    bool flag3 = num >= 1048576L && num < 1073741824L;
                    if (flag3)
                    {
                        result = ((double)num / 1024.0 / 1024.0).ToString("F2") + "MB";
                    }
                    else
                    {
                        bool flag4 = num >= 1073741824L;
                        if (flag4)
                        {
                            result = ((double)num / 1024.0 / 1024.0 / 1024.0).ToString("F2") + "GB";
                        }
                    }
                }
            }
            return result;
        }


        /// <summary>
        /// 计算剩余时间
        /// </summary>
        /// <param name="TotalSize">文件总大小</param>
        /// <param name="TotalDownload">下载了多少byte</param>
        /// <param name="Speed">下载速度</param>
        /// <returns></returns>
        internal static string 返回剩余时长(long TotalSize, long TotalDownload, int Speed)
        {
            string result;
            try
            {
                long num = (TotalSize - TotalDownload) / (long)Speed;
                long num2 = num / 60L / 60L;
                long num3 = num / 60L % 60L;
                long num4 = num - num2 * 60L * 60L - num3 * 60L;
                string text = string.Format("2019-10-11 {0}:{1}:{2}", num2, num3, num4);
                text = Convert.ToDateTime(text).ToString("HH:mm:ss");
                result = text;
            }
            catch (Exception)
            {
                result = "";
            }
            return result;
        }

        /// <summary>
        /// 转int
        /// </summary>
        /// <param name="value">数据</param>
        /// <param name="error">如果错误，则返回此值</param>
        /// <returns>返回int类型</returns>
        public static int ToInt(object value, int error = 0)
        {
            try
            {
                return Convert.ToInt32(value);
            }
            catch (Exception)
            {
                return error;
            }
        }


        /// <summary>
        /// 删除单个文件文件或图片
        /// </summary>
        /// <param name="Path">路径</param>
        /// <returns></returns>
        public static bool DelFillOne(string Path)
        {
            try
            {
                bool ist = false;
                System.IO.FileInfo file = new System.IO.FileInfo(Path);
                if (file.Exists)
                {
                    file.Delete();
                    ist = true;
                }
                return ist;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        /// <summary>
        /// 创建文件夹, 目录如果不存在则新建之 
        /// </summary>
        /// <param name="Path">路径</param>
        /// <returns></returns>
        public static bool AddFill(string Path)
        {
            bool ist = false;
            // 判断目标目录是否存在如果不存在则新建之 
            if (!Directory.Exists(Path))
            {
                Directory.CreateDirectory(Path);
                ist = true;
            }
            else { ist = false; }
            return ist;
        }

    }
}
