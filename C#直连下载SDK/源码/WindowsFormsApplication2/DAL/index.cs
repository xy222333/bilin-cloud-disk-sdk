﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    /// <summary>
    /// 大文件下载
    /// </summary>
    public class index
    {
        private List<model> lis = new List<model>();



        /// <summary>
        /// 下载，返回下载的guid
        /// </summary>
        /// <param name="下载地址"></param>
        /// <param name="保存路径">目录名，最后必须带上斜杠"\"</param>
        /// <param name="文件名"></param>
        /// <returns>返回此条下载的标识</returns>
        public string 下载(string 下载地址, string 保存路径, string 文件名)
        {
            godown d = new godown(下载地址, 保存路径, 文件名);
            model m = d.star();
            lis.Add(m);
            return m.guid;
        }
        /// <summary>
        /// 返回下载的任务数
        /// </summary>
        /// <param name="状态id">【默认返回所有】0=未下载，1=下载中，2=下载完成，3=错误</param>
        /// <returns></returns>
        public int 任务数(int 状态id=-1)
        {
            if (状态id == -1) { return lis.Count; }
            return lis.Where(p=>p.状态== 状态id).ToList().Count;
        }

        /// <summary>
        /// 返回总进度
        /// </summary>
        /// <returns></returns>
        public int 返回总进度()
        {
            int z = 0;
            for (int i = 0; i < lis.Count; i++)
            {
                if (lis[i].状态 == 1)
                {
                    z += lis[i].进度;
                }
            }
            if (任务数(1) == 0) { return 0; }
            return z / 任务数(1);
        }


        /// <summary>
        /// 返回总下载速度
        /// </summary>
        /// <returns></returns>
        public string 返回总下载速度_简称()
        {
            long z = 0;
            for (int i = 0; i < lis.Count; i++)
            {
                if (lis[i].状态 == 1)
                {
                    z += lis[i].下载速度_Byte;
                }
            }
            if (z == 0) { return "0/KB"; }
            return DAL.tool.计算大小(z);
        }

        /// <summary>
        /// 任务查询
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public model 查询任务信息(string guid)
        {
            var a = lis.Where(p => p.guid == guid).ToArray();
            if (a.Length > 0)
            {
                return a[0];
            }
            return null;
        }
        /// <summary>
        /// 取消下载
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="isdelFile">是否删除未下载完文件</param>
        /// <returns></returns>
        public bool 取消下载(string guid, bool isdelFile = false)
        {
            var a = lis.Where(p => p.guid == guid).ToArray();
            if (a.Length > 0)
            {
                a[0].wc.CancelAsync();
                a[0].wc.Dispose();
                a[0].wc = null;
                if (a[0].状态 != 2 && isdelFile == true)
                {
                    new System.Threading.Thread(() =>
                    {
                        try
                        {
                            System.Threading.Thread.Sleep(10);
                            tool.DelFillOne(a[0].保存路径 + a[0].文件名);
                        }
                        catch (Exception)
                        {

                        }
                    }).Start();



                    
                }
                lis.RemoveAll(p => p.guid == guid);
                return true;
            }
            return false;
        }


    }
}
