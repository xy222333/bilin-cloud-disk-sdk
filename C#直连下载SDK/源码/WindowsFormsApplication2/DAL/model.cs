﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DAL
{
    public class model
    {
        /// <summary>
        /// 下载主键，根据这个主键查询
        /// </summary>
        public string guid { get; set; }

        public string 下载地址 { get; set; }


        /// <summary>
        /// 目录名，最后必须带上斜杠
        /// </summary>
        public string 保存路径 { get; set; }

        public string 文件名 { get; set; }

        public long 下载速度_Byte { get; set; }
        public string 下载速度_简文 { get { return tool.计算大小(下载速度_Byte); } }

        public string 剩余时间 { get { return tool.返回剩余时长(文件总大小Byte, 下载了多少Byte, tool.ToInt(下载速度_Byte)); } }

        /// <summary>
        /// 0-100
        /// </summary>
        public int 进度 { get; set; }

        /// <summary>
        /// 0=未下载，1=下载中，2=下载完成，3=错误
        /// </summary>
        public int 状态 { get; set; }

        /// <summary>
        /// 状态=3的时候此处才有信息
        /// </summary>
        public string 错误信息 { get; set; }

        public long 下载了多少Byte { get; set; }

        public string 下载了多少_简文 { get { return tool.计算大小(下载了多少Byte); } }

        public long 文件总大小Byte { get; set; }

        public string 文件总大小_简文 { get { return tool.计算大小(文件总大小Byte); } }

        public WebClient wc { get; set; }
    }
}
